package lexer

import (
	"fmt"
	"strconv"
	"unicode"
	"unicode/utf8"

	"jsInterpreter/src/utils"
)

type LexalType = uint8

var Keywords = map[string]LexalType{
	// Reserved words
	"function": LFUNCTION,
	"let":      LLET,
	"var":      LVAR,
	"const":    LCONST,
	"NaN":      LSPECIAL,
	"Infinity": LSPECIAL,
	"console":  LIDENTIFIER,
	"true":     LSPECIAL,
	"false":    LSPECIAL,
	"if":       LIF,
	"else":     LELSE,
	"return":   LRETURN,
}

// KeywordData is a very broad categorization of keywords.
// So we're going to just use to let it define a func and pass the *lexer pointer true.
// So the keyword can figure out where to go.

var KeywordData = map[string]func(*Lexer) LexalType{
	"NaN": func(lexer *Lexer) LexalType {
		lexer.TokenNumber = utils.GNaN
		return LINTEGER
	},
	"Infinity": func(lexer *Lexer) LexalType {
		lexer.TokenNumber = utils.GInfinity
		return LINTEGER
	},
	"true": func(lexer *Lexer) LexalType {
		lexer.TokenBool = true
		return LBOOL
	},
	"false": func(lexer *Lexer) LexalType {
		lexer.TokenBool = false
		return LBOOL
	},
}

// LexalMap with a all lexals, and their respective characters.
var LexalMap = map[LexalType]string{
	LEOF:           "end of file",
	LINTEGER:       "number",
	LPLUS:          "\"+\"",
	LEQUALPLUS:     "\"+=\"",
	LEQUALMIN:      "\"-=\"",
	LMIN:           "\"-\"",
	LASTERISK:      "\"*\"",
	LEQUALASTERISK: "\"*=\"",
	LEQUALDIVIDER:  "\"/=\"",
	LDIVIDER:       "\"/\"",
	LLPAREN:        "\"(\"",
	LRPAREN:        "\")\"",
	LLBRACE:        "\"{\"",
	LRBRACE:        "\"}\"",
	LFUNCTION:      "\"function\"",
	LLET:           "\"let\"",
	LVAR:           "\"var\"",
	LCONST:         "\"const\"",
	LIDENTIFIER:    "identifier",
	LEQUAL:         "\"=\"",
	LSEMI:          "\";\"",
	LNEWLINE:       "newline",
	LSTRING:        "string",
	LCOMMA:         "\",\"",
	LDOT:           "\"\".\"",
	LBOOL:          "boolean",
	LIF:            "\"if\"",
	LELSE:          "\"else\"",
	LRETURN:        "\"return\"",
	LCOLON:         "\":\"",
	LSPECIAL:       "INCORRECT",
	LPLUSPLUS:      "\"++\"",
	LMINMIN:        "\"--\"",
	LEXPONENT:      "\"**\"",
	LEQUALEXPONENT: "\"**=\"",
}

const (
	LEOF LexalType = iota + 1
	LINTEGER
	LBIGINTEGER
	LPLUS
	LEQUALPLUS
	LEQUALMIN
	LMIN
	LASTERISK
	LEQUALASTERISK
	LEQUALDIVIDER
	LDIVIDER
	LLPAREN
	LRPAREN
	LLBRACE
	LRBRACE
	LFUNCTION
	LLET
	LCONST
	LVAR
	LIDENTIFIER
	LEQUAL
	LEQUALEQUAL
	LNOTEQUAL
	LSEMI
	LNEWLINE
	LSTRING
	LDOT
	LCOMMA
	LBOOL
	LIF
	LELSE
	LRETURN
	LCOLON

	LGREATER
	LGREATEREQUAL
	LLESS
	LLESSEQUAL

	LSPECIAL
	LPLUSPLUS
	LMINMIN

	LEXPONENT
	LEQUALEXPONENT
)

type Lexer struct {
	text        string
	TokenString string
	TokenNumber float64
	position    int
	prev        int
	currentChar rune
	TokenBool   bool
}

func NewLexer(codeText string) *Lexer {
	lexer := &Lexer{
		text: codeText,
	}

	lexer.advance()
	return lexer
}

func (lexer *Lexer) consumeString() string {
	beginPos := lexer.prev
consuming:
	for {
		switch lexer.currentChar {
		case 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm',
			'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z',
			'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M',
			'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z':
			lexer.advance()
		default:
			break consuming
		}
	}

	return lexer.raw(beginPos, lexer.prev)
}

func (lexer *Lexer) advance() {
	if lexer.position >= len(lexer.text) {
		lexer.currentChar = zeroByte
		lexer.prev = lexer.position
		return
	}
	// Fast-path for normal ASCII
	newChar, width := rune(lexer.text[lexer.position]), 1

	// Well, we have to revert back to this...
	if newChar >= utf8.RuneSelf {
		newChar, width = utf8.DecodeRuneInString(lexer.text[lexer.position:])
	}

	lexer.currentChar = newChar
	lexer.prev = lexer.position
	lexer.position += width
}

func (lexer *Lexer) raw(start, end int) string {
	return lexer.text[start:end]
}

// This function also should be able to parse LDOT.
// Because .9 and console.log is somewhat similar but hard to distract.
// From lexing.
// Then it should be able to lex like: https://tc39.es/ecma262/#sec-literals-numeric-literals
func (lexer *Lexer) parseNumber() LexalType {
	firstChar := lexer.currentChar
	firstCharPos := lexer.prev
	lexer.advance()

	// If the first character a '.', but don't trail with numbers.
	// We know it's LDOT.
	if firstChar == '.' && (lexer.currentChar > '9' || lexer.currentChar < '0') {
		return LDOT
	}

	// Account for all underscores in a number.
	underscoreCount := 0

	// Numeric literal allows for the base to be changed.
	// e.g. binary, hex, octal.
	// Base(maths): https://simple.wikipedia.org/wiki/Base_(mathematics)
	base := 0.0

	// Remember when the last underscore was found.
	// So we can check if a number has only one numeric separator.
	lastUnderscorePos := 0

	// Specify if current number is a legacy Octal Literal.
	// Ref: https://tc39.es/ecma262/#prod-LegacyOctalIntegerLiteral
	legacyOctalLiteral := false

	// Specify if the first digit is a dot
	hasDotOrExponent := firstChar == '.'

	if firstChar == '0' {
		switch lexer.currentChar {
		case 'x', 'X': // 0x (https://tc39.es/ecma262/#prod-HexIntegerLiteral)
			base = 16
		case 'b', 'B': // 0b (https://tc39.es/ecma262/#prod-BinaryIntegerLiteral)
			base = 2
		case 'o', 'O': // 0o (https://tc39.es/ecma262/#prod-OctalIntegerLiteral)
			base = 8
		case '0', '1', '2', '3', '4', '5', '6', '7', '_': // (https://tc39.es/ecma262/#prod-LegacyOctalLikeDecimalIntegerLiteral)
			base = 8
			legacyOctalLiteral = true
		case '8', '9': // (https://tc39.es/ecma262/#prod-NonOctalDecimalIntegerLiteral)
			legacyOctalLiteral = true
		}
	}

	// Base has changed? this means :D
	// we have to work with some custom bases.
	if base != 0 {
		// Just a simple variable to tell if the current lexing number
		// is the first number(syntax lexing/erroring).
		isFirstNumber := true
		// 'reset' the number, as it will be avoided to create a new variable.
		// And instead just evolute this property.
		lexer.TokenNumber = 0

		// Specify if the current number is a invalid legacy octal literal
		// This is use for some slow path parsing of the numbers.
		isInvalidLegacyOctalLiteral := false

		// Skip the first 0, because it's kinda worthless.
		if !legacyOctalLiteral {
			lexer.advance()
		}

		// This loop should parse each non-10 base number.
		// Thank you Evan/ESBuild for the logic.
	numberLiteral:
		for {
			switch lexer.currentChar {
			// In each non-10 base literal, the 0 and 1 are allowed and can be used
			// without any checks.
			case '0', '1':
				lexer.TokenNumber = lexer.TokenNumber*base + float64(lexer.currentChar-'0')

				// These numbers are still allowed, unless for base-2(binary).
			case '2', '3', '4', '5', '6', '7':
				if base == 2 {
					lexer.SyntaxError()
				}
				lexer.TokenNumber = lexer.TokenNumber*base + float64(lexer.currentChar-'0')

				// 8 and 9 are a bit tricker. As they are only allowed on base < 10.
			// And if legacyOctalLiteral is true, it  would indicate a invalid
			// legacy octal literal
			case '8', 9:
				if legacyOctalLiteral {
					isInvalidLegacyOctalLiteral = false
				}
				if base < 10 {
					lexer.SyntaxError()
				}

			// Now these should only be parsed for hex(base-16)
			// But require 2 different switches to account for {lower,upper}-case.
			case 'A', 'B', 'C', 'D', 'E', 'F':
				if base != 16 {
					lexer.SyntaxError()
				}
				lexer.TokenNumber = lexer.TokenNumber*base + float64(lexer.currentChar+10-'A')

			case 'a', 'b', 'c', 'd', 'e', 'f':
				if base != 16 {
					lexer.SyntaxError()
				}
				lexer.TokenNumber = lexer.TokenNumber*base + float64(lexer.currentChar+10-'a')

			// Another special case!
			// _ are allowed in any number literal :D
			// And is nothing more then a numeric separator.
			case '_':
				// Check if the numeric separator is twice in a row.
				if lastUnderscorePos > 0 && lexer.prev == lastUnderscorePos+1 {
					lexer.SyntaxError()
				}

				// The first digit must exist, as the numeric separator cannot be started with.
				if isFirstNumber || legacyOctalLiteral {
					lexer.SyntaxError()
				}

				lastUnderscorePos = lexer.prev
				underscoreCount++

			default:
				// The first digit must exist
				if isFirstNumber {
					lexer.SyntaxError()
				}

				break numberLiteral
			}

			lexer.advance()
			isFirstNumber = false
		}

		isBigIntegerLiteral := lexer.currentChar == 'n' && !hasDotOrExponent

		// Slow path: do we need to re-scan the input as text?
		if isBigIntegerLiteral || isInvalidLegacyOctalLiteral {
			text := lexer.raw(firstCharPos, lexer.prev)

			// Can't use a leading zero for bigint literals
			if isBigIntegerLiteral && legacyOctalLiteral {
				lexer.SyntaxError()
			}

			// Filter out underscores
			if underscoreCount > 0 {
				bytes := make([]byte, 0, len(text)-underscoreCount)
				for i := 0; i < len(text); i++ {
					c := text[i]
					if c != '_' {
						bytes = append(bytes, c)
					}
				}
				text = utils.UnsafeString(bytes)
			}

			// Store bigints as text to avoid precision loss
			if isBigIntegerLiteral {
				lexer.TokenString = text
			} else if isInvalidLegacyOctalLiteral {
				// Legacy octal literals may turn out to be a base 10 literal after all
				value, _ := strconv.ParseFloat(text, 64)
				lexer.TokenNumber = value
			}
		}
	} else {
		// Floating-point literal
		isInvalidLegacyOctalLiteral := firstChar == '0' && (lexer.currentChar == '8' || lexer.currentChar == '9')

		// Initial digits
		for {
			if lexer.currentChar < '0' || lexer.currentChar > '9' {
				if lexer.currentChar != '_' {
					break
				}

				// Cannot have multiple underscores in a row
				if lastUnderscorePos > 0 && lexer.prev == lastUnderscorePos+1 {
					lexer.SyntaxError()
				}

				// The specification forbids underscores in this case
				if isInvalidLegacyOctalLiteral {
					lexer.SyntaxError()
				}

				lastUnderscorePos = lexer.prev
				underscoreCount++
			}
			lexer.advance()
		}

		// Fractional digits
		if firstChar != '.' && lexer.currentChar == '.' {
			// An underscore must not come last
			if lastUnderscorePos > 0 && lexer.prev == lastUnderscorePos+1 {
				lexer.SyntaxError()
			}

			hasDotOrExponent = true
			lexer.advance()
			if lexer.currentChar == '_' {
				lexer.SyntaxError()
			}
			for {
				if lexer.currentChar < '0' || lexer.currentChar > '9' {
					if lexer.currentChar != '_' {
						break
					}

					// Cannot have multiple underscores in a row
					if lastUnderscorePos > 0 && lexer.prev == lastUnderscorePos+1 {
						lexer.SyntaxError()
					}

					lastUnderscorePos = lexer.prev
					underscoreCount++
				}
				lexer.advance()
			}
		}

		// Exponent
		if lexer.currentChar == 'e' || lexer.currentChar == 'E' {
			// An underscore must not come last
			if lastUnderscorePos > 0 && lexer.prev == lastUnderscorePos+1 {
				lexer.SyntaxError()
			}

			hasDotOrExponent = true
			lexer.advance()
			if lexer.currentChar == '+' || lexer.currentChar == '-' {
				lexer.advance()
			}
			if lexer.currentChar < '0' || lexer.currentChar > '9' {
				lexer.SyntaxError()
			}
			for {
				if lexer.currentChar < '0' || lexer.currentChar > '9' {
					if lexer.currentChar != '_' {
						break
					}

					// Cannot have multiple underscores in a row
					if lastUnderscorePos > 0 && lexer.prev == lastUnderscorePos+1 {
						lexer.SyntaxError()
					}

					lastUnderscorePos = lexer.prev
					underscoreCount++
				}
				lexer.advance()
			}
		}

		// Take a slice of the text to parse
		text := lexer.raw(firstCharPos, lexer.prev)

		// Filter out underscores
		if underscoreCount > 0 {
			bytes := make([]byte, 0, len(text)-underscoreCount)
			for i := 0; i < len(text); i++ {
				c := text[i]
				if c != '_' {
					bytes = append(bytes, c)
				}
			}
			text = utils.UnsafeString(bytes)
		}

		if lexer.currentChar == 'n' && !hasDotOrExponent {
			// The only bigint literal that can start with 0 is "0n"
			if len(text) > 1 && firstChar == '0' {
				lexer.SyntaxError()
			}

			// Store bigints as text to avoid precision loss
			lexer.TokenString = text
		} else if !hasDotOrExponent && lexer.prev-firstCharPos < 10 {
			// Parse a 32-bit integer (very fast path)
			var number uint32
			for _, c := range text {
				number = number*10 + uint32(c-'0')
			}
			lexer.TokenNumber = float64(number)
		} else {
			// Parse a double-precision floating-point number
			value, _ := strconv.ParseFloat(text, 64)
			lexer.TokenNumber = value
		}
	}

	// An underscore must not come last
	if lastUnderscorePos > 0 && lexer.prev == lastUnderscorePos+1 {
		lexer.SyntaxError()
	}

	// Handle bigint literals after the underscore-at-end check above
	if lexer.currentChar == 'n' && !hasDotOrExponent {
		lexer.advance()
		return LBIGINTEGER
	}

	// Identifiers can't occur immediately after numbers
	if IsIdentifierStart(lexer.currentChar) {
		lexer.SyntaxError()
	}

	return LINTEGER
}

func IsIdentifierStart(codePoint rune) bool {
	switch codePoint {
	case '_', '$',
		'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm',
		'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z',
		'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M',
		'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z':
		return true
	}

	// All ASCII identifier start code points are listed above
	if codePoint < 0x7F {
		return false
	}

	return unicode.Is(idStart, codePoint)
}

func (lexer *Lexer) SyntaxError() {
	panic("Syntax error: unexpected end of file")
}

const zeroByte = -1

func (lexer *Lexer) GetNextToken() LexalType {
	switch lexer.currentChar {
	case zeroByte:
		return LEOF
	case '\t', ' ':
		lexer.advance()
		token := lexer.GetNextToken()
		return token
	case '\n':
		lexer.advance()
		return LNEWLINE
	case '.', '0', '1', '2', '3', '4', '5', '6', '7', '8', '9':
		return lexer.parseNumber()

	case 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm',
		'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z',
		'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M',
		'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z':
		consumedString := lexer.consumeString()
		keyword, ok := Keywords[consumedString]
		lexer.TokenString = consumedString
		if !ok {
			return LIDENTIFIER
		}
		if keyword == LSPECIAL {
			dataFunc := KeywordData[consumedString]
			return dataFunc(lexer)
		}

		return keyword
	case '+':
		lexer.advance()
		if lexer.currentChar == '=' {
			lexer.advance()
			return LEQUALPLUS
		} else if lexer.currentChar == '+' {
			lexer.advance()
			return LPLUSPLUS
		}
		return LPLUS
	case '*':
		lexer.advance()
		if lexer.currentChar == '=' {
			lexer.advance()
			return LEQUALASTERISK
		} else if lexer.currentChar == '*' {
			lexer.advance()
			if lexer.currentChar == '=' {
				lexer.advance()
				return LEQUALEXPONENT
			}
			return LEXPONENT
		}
		return LASTERISK
	case '-':
		lexer.advance()
		if lexer.currentChar == '=' {
			lexer.advance()
			return LEQUALMIN
		} else if lexer.currentChar == '-' {
			lexer.advance()
			return LMINMIN
		}
		return LMIN
	case '/':
		lexer.advance()
		if lexer.currentChar == '=' {
			lexer.advance()
			return LEQUALDIVIDER
		}
		return LDIVIDER
	case '(':
		lexer.advance()
		return LLPAREN
	case ')':
		lexer.advance()
		return LRPAREN
	case '{':
		lexer.advance()
		return LLBRACE
	case '}':
		lexer.advance()
		return LRBRACE
	case '=':
		lexer.advance()
		if lexer.currentChar == '=' {
			lexer.advance()
			return LEQUALEQUAL
		}
		return LEQUAL
	case ';':
		lexer.advance()
		return LSEMI
	case '"', '\'':
		// If it starts with with " or ' we should consume a string.
		// until EOF or until " or ' is found again.

		// Advance to the next character. So we skip the first " or '
		quoteChar := lexer.currentChar
		lexer.advance()
		startPos := lexer.prev
		for lexer.currentChar != zeroByte && lexer.currentChar != quoteChar {
			lexer.advance()
		}
		lexer.TokenString = lexer.raw(startPos, lexer.prev)
		// Advance to the next character. So we skip the last " or '
		lexer.advance()
		return LSTRING
	case ',':
		lexer.advance()
		return LCOMMA
	case ':':
		lexer.advance()
		return LCOLON
	case '>':
		lexer.advance()
		if lexer.currentChar == '=' {
			lexer.advance()
			return LGREATEREQUAL
		}
		return LGREATER
	case '<':
		lexer.advance()
		if lexer.currentChar == '=' {
			lexer.advance()
			return LLESSEQUAL
		}
		return LLESS
	case '!':
		lexer.advance()
		if lexer.currentChar == '=' {
			lexer.advance()
			return LNOTEQUAL
		}
		lexer.SyntaxError()
		return LEOF
	default:
		panic(fmt.Sprintf("Unrecognized input: %s, col: %d", string(lexer.currentChar), lexer.position))
	}
}
