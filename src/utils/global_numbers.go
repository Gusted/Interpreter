package utils

import "math"

var (
	GNaN      = math.NaN()
	GInfinity = math.Inf(1)
)
