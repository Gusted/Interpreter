package errors

import (
	"errors"
	"fmt"
)

var (
	errKindNotImplemented = errors.New("AST Kind not implemented, ")

	errReferenceError = errors.New("ReferenceError: variable is not defined: ")
)

func ErrKindNotImplemented(kind interface{}) error {
	return fmt.Errorf(`%w: %d`, errKindNotImplemented, kind)
}

func ErrReferenceError(variable string) error {
	return fmt.Errorf(`%w: "%s"`, errReferenceError, variable)
}
