package parser

import (
	"fmt"

	"jsInterpreter/src/lexer"
)

type NodeStruct struct{}

type Node interface{}

type Expr interface {
	Node
	expr()
}

type Declr interface {
	Node
	declr()
}

type NOOP interface {
	Node
	noop()
}

func (BinaryOperation) expr()         {}
func (BinaryLeftSideOperation) expr() {}
func (EqualCheck) expr()              {}
func (UnaryOperation) expr()          {}
func (Number) expr()                  {}
func (Boolean) expr()                 {}
func (MemberAccessProperty) expr()    {}
func (String) expr()                  {}
func (Identifier) expr()              {}
func (MemberAccessFunc) expr()        {}
func (Compound) expr()                {}
func (FunctionCall) expr()            {}
func (ObjectExpr) expr()              {}
func (CompareOperation) expr()        {}
func (InDecrement) expr()             {}

func (Assign) declr()    {}
func (Redeclare) declr() {}
func (IfBlock) declr()   {}

func (ObjectEntry) noop() {}

type BinaryOperation struct {
	Left      Expr
	Right     Expr
	Operation lexer.LexalType
}

type BinaryLeftSideOperation struct {
	Right     Expr
	Left      Identifier
	Operation lexer.LexalType
}

type Number struct {
	Value float64
}

type UnaryOperation struct {
	Expr      Expr
	Operation lexer.LexalType
}

// Represents a function() { ... }.
type Compound struct {
	Identifier string
	Children   []Node
	Arguments  []string
	Toplevel   bool
}

type Assign struct {
	Right       Node
	Identifier  string
	DeclareType VarDeclare
}

type VarDeclare uint8

const (
	VVar VarDeclare = iota + 1
	VConst
	VLet
)

type FunctionCall struct {
	Identifier string
	Arguments  []Node
}

type String struct {
	Value string
}

type NoOp struct{}

type MemberAccessProperty struct {
	Left       Node
	Identifier string
}

type MemberAccessFunc struct {
	Left       Node
	Identifier string
	Arguments  []Node
}

type EqualCheck struct {
	Left      Node
	Operation lexer.LexalType
	Right     Node
}

type Boolean struct {
	Value bool
}

type Identifier struct {
	Identifier string
}

type Redeclare struct {
	Right      Expr
	Identifier Identifier
}

type IfItem struct {
	Condition Expr
	Action    []Node
	IsElse    bool
}

type IfBlock struct {
	Items []IfItem
}

type ReturnStatement struct {
	ReturnExpr Expr
}

type ObjectEntry struct {
	Value Node
	Key   string
}

type ObjectExpr struct {
	Entries []ObjectEntry
}

type CompareOperation struct {
	Left  Node
	Right Node
	Op    lexer.LexalType
}

type InDecrement struct {
	Identifier Identifier
	Increment  bool
}

func newNumber(numberValue float64) Number {
	return Number{
		Value: numberValue,
	}
}

func newBool(value bool) Boolean {
	return Boolean{
		Value: value,
	}
}

func newUnaryOperation(operation lexer.LexalType, expr Expr) UnaryOperation {
	return UnaryOperation{
		Operation: operation,
		Expr:      expr,
	}
}

func newBinaryOperation(left, right Expr, op lexer.LexalType) BinaryOperation {
	return BinaryOperation{
		Left:      left,
		Operation: op,
		Right:     right,
	}
}

func newBinaryLeftSideOperation(left Identifier, right Expr, op lexer.LexalType) BinaryLeftSideOperation {
	return BinaryLeftSideOperation{
		Left:      left,
		Operation: op,
		Right:     right,
	}
}

func newCompund(children []Node, arguments []string, topLevel bool, identifier string) Compound {
	return Compound{
		Identifier: identifier,
		Children:   children,
		Arguments:  arguments,
		Toplevel:   topLevel,
	}
}

func newAssignment(left string, declare VarDeclare, right Node) Assign {
	return Assign{
		Identifier:  left,
		DeclareType: declare,
		Right:       right,
	}
}

func newFunctionCall(identifier string, arguments []Node) FunctionCall {
	return FunctionCall{
		Identifier: identifier,
		Arguments:  arguments,
	}
}

func newString(stringValue string) String {
	return String{
		Value: stringValue,
	}
}

func newMemberAccessProperty(identifier string, left Node) MemberAccessProperty {
	return MemberAccessProperty{
		Identifier: identifier,
		Left:       left,
	}
}

func newMemberAccessFunc(left Node, identifier string, arguments []Node) MemberAccessFunc {
	return MemberAccessFunc{
		Left:       left,
		Identifier: identifier,
		Arguments:  arguments,
	}
}

func newEqualsCheck(left, right Node, operation lexer.LexalType) EqualCheck {
	return EqualCheck{
		Left:      left,
		Operation: operation,
		Right:     right,
	}
}

func newIdentifier(identifier string) Identifier {
	return Identifier{
		Identifier: identifier,
	}
}

func newRedeclare(identifier Identifier, right Expr) Redeclare {
	return Redeclare{
		Identifier: identifier,
		Right:      right,
	}
}

func newIfBlock(items []IfItem) IfBlock {
	return IfBlock{
		Items: items,
	}
}

func newReturnStmt(returnExpr Expr) ReturnStatement {
	return ReturnStatement{
		ReturnExpr: returnExpr,
	}
}

func newObjetExpr(entries []ObjectEntry) ObjectExpr {
	return ObjectExpr{
		Entries: entries,
	}
}

func newComparison(left, right Node, op lexer.LexalType) CompareOperation {
	return CompareOperation{
		Left:  left,
		Right: right,
		Op:    op,
	}
}

func newInDecrement(identifier Identifier, increment bool) InDecrement {
	return InDecrement{
		Identifier: identifier,
		Increment:  increment,
	}
}

type Parser struct {
	lexer        *lexer.Lexer
	currentToken lexer.LexalType
}

func NewParser(parser *lexer.Lexer) *Parser {
	return &Parser{
		lexer:        parser,
		currentToken: parser.GetNextToken(),
	}
}

func (parser *Parser) eat(token lexer.LexalType, expectedErrorMessage ...interface{}) {
	if parser.currentToken == token {
		parser.currentToken = parser.lexer.GetNextToken()
		return
	}
	if len(expectedErrorMessage) > 0 {
		panic(expectedErrorMessage[0])
	}
	panic(fmt.Sprintf("Unrecognized current input: %s, expected: %s", lexer.LexalMap[parser.currentToken], lexer.LexalMap[token]))
}

func (parser *Parser) eatSoft(token lexer.LexalType) {
	if parser.currentToken == token {
		parser.currentToken = parser.lexer.GetNextToken()
	}
}

// function a(b, c) {}
func (parser *Parser) funcDefArguments() []string {
	parser.eat(lexer.LLPAREN)
	args := []string{}
	for parser.currentToken != lexer.LRPAREN {
		switch parser.currentToken {
		case lexer.LIDENTIFIER:
			parser.eat(lexer.LIDENTIFIER)
			args = append(args, parser.lexer.TokenString)
		default:
			panic(fmt.Sprintf("Unrecognized current input: %s, expected: %s", lexer.LexalMap[parser.currentToken], lexer.LexalMap[lexer.LIDENTIFIER]))
		}

		if parser.currentToken == lexer.LCOMMA {
			parser.eat(lexer.LCOMMA)
		} else if parser.currentToken != lexer.LRPAREN {
			panic(fmt.Sprintf("Unrecognized current input: %s, expected: %s", lexer.LexalMap[parser.currentToken], lexer.LexalMap[lexer.LRPAREN]))
		}
	}
	parser.eat(lexer.LRPAREN)

	return args
}

// a(b, c)
//  ^^^^^^
// the arguments can vary, from identifier to a function call or even a string
// and a function call can be followed by a member access or even complex numbers.
func (parser *Parser) arguments() []Node {
	parser.eat(lexer.LLPAREN)
	var args []Node
	for parser.currentToken != lexer.LRPAREN {
		args = append(args, parser.expr())
		if parser.currentToken == lexer.LCOMMA {
			parser.eat(lexer.LCOMMA)
		} else if parser.currentToken != lexer.LRPAREN {
			panic(fmt.Sprintf("Unrecognized current input: %s, expected: %s", lexer.LexalMap[parser.currentToken], lexer.LexalMap[lexer.LRPAREN]))
		}
	}
	parser.eat(lexer.LRPAREN)
	return args
}

func (parser *Parser) compoundStatement() Expr {
	// function  test        (       )       {       .... }
	// LFUNCTION LIDENTIFIER LLPAREN LRPAREN LLBRACE .... LRBRACE
	parser.eat(lexer.LFUNCTION)
	identifier := parser.lexer.TokenString
	parser.eat(lexer.LIDENTIFIER)
	arguments := parser.funcDefArguments()
	parser.eat(lexer.LLBRACE)
	parser.eatSoft(lexer.LNEWLINE)
	nodes := parser.statementList()

	parser.eat(lexer.LRBRACE)

	return newCompund(nodes, arguments, false, identifier)
}

func (parser *Parser) statementList() []Node {
	if parser.currentToken == lexer.LRBRACE {
		return []Node{}
	}

	result := []Node{}
	node := parser.statement()
	result = append(result, node)

	for parser.currentToken == lexer.LSEMI || parser.currentToken == lexer.LNEWLINE {
		parser.eat(parser.currentToken)
	checkEnd:
		if parser.currentToken == lexer.LRBRACE || parser.currentToken == lexer.LEOF {
			break
		}
		// let a = 1;\n
		if parser.currentToken == lexer.LNEWLINE {
			parser.eat(lexer.LNEWLINE)
			goto checkEnd
		}
		result = append(result, parser.statement())
	}
	if parser.currentToken == lexer.LIDENTIFIER {
		panic("SyntaxError: Unexpected token, identifier")
	}
	return result
}

func (parser *Parser) identifierExpr() Expr {
	identifier := parser.lexer.TokenString
	parser.eat(lexer.LIDENTIFIER)
	switch parser.currentToken {
	case lexer.LLPAREN:
		arguments := parser.arguments()
		return newFunctionCall(identifier, arguments)
	default:
		identifier := newIdentifier(identifier)
		if parser.currentToken == lexer.LDOT {
			return parser.parseSuffix(identifier)
		}
		return identifier
	}
}

func (parser *Parser) identifier(isFunction bool) Node {
	identifier := parser.lexer.TokenString
	parser.eat(lexer.LIDENTIFIER)
	switch parser.currentToken {
	case lexer.LLPAREN:
		arguments := parser.arguments()
		return newFunctionCall(identifier, arguments)
	case lexer.LEQUALASTERISK, lexer.LEQUALDIVIDER, lexer.LEQUALMIN, lexer.LEQUALPLUS, lexer.LEQUALEXPONENT:
		token := parser.currentToken
		parser.eat(token)
		return newBinaryLeftSideOperation(newIdentifier(identifier), parser.term(), token)
	default:
		identifier := newIdentifier(identifier)
		if isFunction {
			return identifier
		}

		// variable = ???
		if parser.currentToken == lexer.LEQUAL {
			parser.eat(lexer.LEQUAL)
			var right Expr
			if parser.currentToken == lexer.LFUNCTION {
				right = parser.compoundStatement()
			} else {
				right = parser.expr()
			}
			return newRedeclare(identifier, right)
			// Let's assume for now they just want to call the variable.
			// AKA just display that value? only useful for CLI usage
		}
		return parser.parseSuffix(identifier)
	}
}

func (parser *Parser) SyntaxError() {
	panic("Syntax error: unexpected token " + lexer.LexalMap[parser.currentToken])
}

// if (...) { ... }.
// We also should cover the case of if (...) { ... } else { ... }.
// And the case of if (...) { ... } else if (...) { ... } else { ... }.
func (parser *Parser) ifStatement() Node {
	var items []IfItem

	// Get the first `if (...) { ... }`.
	parser.eat(lexer.LIF)
	parser.eat(lexer.LLPAREN)
	condition := parser.expr()
	parser.eat(lexer.LRPAREN)
	parser.eat(lexer.LLBRACE)
	parser.eatSoft(lexer.LNEWLINE)

	items = append(items, IfItem{condition, parser.statementList(), false})
	parser.eat(lexer.LRBRACE)
	noDoubleElse := false

	for parser.currentToken == lexer.LELSE {
		isElse := true
		parser.eat(lexer.LELSE)
		if parser.currentToken == lexer.LIF {
			isElse = false
			parser.eat(lexer.LIF)
		} else {
			if noDoubleElse {
				parser.SyntaxError()
			}
			noDoubleElse = true
		}
		var condition Expr
		if !isElse {
			parser.eat(lexer.LLPAREN)
			condition = parser.expr()
			parser.eat(lexer.LRPAREN)
		}
		parser.eat(lexer.LLBRACE)
		parser.eatSoft(lexer.LNEWLINE)
		items = append(items, IfItem{condition, parser.statementList(), isElse})
		parser.eat(lexer.LRBRACE)
	}

	return newIfBlock(items)
}

func (parser *Parser) returnStatement() Node {
	parser.eat(lexer.LRETURN)
	expr := parser.expr()
	return newReturnStmt(expr)
}

func (parser *Parser) statement() Node {
	var node Node

	switch parser.currentToken {
	case lexer.LFUNCTION:
		node = parser.compoundStatement()
	case lexer.LIDENTIFIER:
		node = parser.identifier(false)
	case lexer.LCONST, lexer.LLET, lexer.LVAR:
		node = parser.assignmentStatement()
	case lexer.LIF:
		node = parser.ifStatement()
	case lexer.LRETURN:
		node = parser.returnStatement()
	default:
		node = parser.expr()
	}
	return node
}

func (parser *Parser) assignmentStatement() Node {
	leftVariable, declareType := parser.variable()
	parser.eat(lexer.LEQUAL)

	right := parser.expr()
	return newAssignment(leftVariable, declareType, right)
}

func (parser *Parser) getDeclareType() VarDeclare {
	switch parser.currentToken {
	case lexer.LCONST:
		return VConst
	case lexer.LLET:
		return VLet
	case lexer.LVAR:
		return VVar
	default:
		panic("SyntaxError: Unexpected token, expected: const, let or var")
	}
}

func (parser *Parser) variable() (string, VarDeclare) {
	declareType := parser.getDeclareType()
	parser.eat(parser.currentToken)

	stringToken := parser.lexer.TokenString

	parser.eat(lexer.LIDENTIFIER)
	return stringToken, declareType
}

func (parser *Parser) term() Expr {
	node := parser.factor()
	token := parser.currentToken

	for token >= lexer.LASTERISK && token <= lexer.LDIVIDER {
		switch token {
		case lexer.LASTERISK:
			parser.eat(lexer.LASTERISK)
		case lexer.LDIVIDER:
			parser.eat(lexer.LDIVIDER)
		}
		node = newBinaryOperation(node, parser.factor(), token)
		token = parser.currentToken
	}
	if token == lexer.LEQUALEQUAL || token == lexer.LNOTEQUAL {
		parser.eat(token)
		node = newEqualsCheck(node, parser.term(), token)
	}
	return node
}

// Should check for things like "aaa".length.
func (parser *Parser) parseSuffix(node Expr) Expr {
	switch parser.currentToken {
	case lexer.LDOT:
		parser.eat(lexer.LDOT)
		identifier := parser.lexer.TokenString
		parser.eat(lexer.LIDENTIFIER)
		// if current token has () then it is a function call
		if parser.currentToken == lexer.LLPAREN {
			arguments := parser.arguments()
			return newMemberAccessFunc(node, identifier, arguments)
		}
		return newMemberAccessProperty(identifier, node)
	case lexer.LPLUSPLUS, lexer.LMINMIN:
		currentToken := parser.currentToken
		node.expr()
		switch node := node.(type) {
		case Identifier:
			parser.eat(currentToken)
			return newInDecrement(node, currentToken == lexer.LPLUSPLUS)
		default:
			panic("Invalid left-hand side expression in postfix operation")
		}
	case lexer.LEXPONENT:
		parser.eat(lexer.LEXPONENT)
		return newBinaryOperation(node, parser.factor(), lexer.LEXPONENT)
	default:
		return node
	}
}

func (parser *Parser) startObjectParsing() ObjectExpr {
	var nodeEntries []ObjectEntry
	parser.eat(lexer.LLBRACE)
	parser.eatSoft(lexer.LNEWLINE)
	for {
		key := parser.lexer.TokenString
		isIdentifier := parser.currentToken == lexer.LIDENTIFIER
		parser.eat(parser.currentToken)
		// Identifiers can be used as keys and as value at the same time.
		// So if the next token is not a colon, it's a key-valye pair.

		var value Expr
		if isIdentifier && parser.currentToken != lexer.LCOLON {
			value = newIdentifier(key)
		} else {
			parser.eat(lexer.LCOLON)
			value = parser.expr()
		}
		nodeEntries = append(nodeEntries, ObjectEntry{value, key})
		if parser.currentToken != lexer.LCOMMA {
			break
		}
		parser.eat(lexer.LCOMMA)
		parser.eatSoft(lexer.LNEWLINE)
		if parser.currentToken == lexer.LRBRACE {
			break
		}
	}

	parser.eat(lexer.LRBRACE)
	return newObjetExpr(nodeEntries)
}

func (parser *Parser) factor() Expr {
	token := parser.currentToken
	switch token {
	case lexer.LPLUS:
		parser.eat(lexer.LPLUS)
		return newUnaryOperation(token, parser.factor())
	case lexer.LMIN:
		parser.eat(lexer.LMIN)
		return newUnaryOperation(token, parser.factor())
	case lexer.LINTEGER:
		parser.eat(lexer.LINTEGER)
		return parser.parseSuffix(newNumber(parser.lexer.TokenNumber))
	case lexer.LBOOL:
		parser.eat(lexer.LBOOL)
		return newBool(parser.lexer.TokenBool)
	case lexer.LLPAREN:
		parser.eat(lexer.LLPAREN)
		node := parser.expr()
		parser.eat(lexer.LRPAREN)
		return node
	case lexer.LSTRING:
		identifier := parser.lexer.TokenString
		parser.eat(lexer.LSTRING)
		return parser.parseSuffix(newString(identifier))
	case lexer.LIDENTIFIER:
		return parser.identifierExpr()
	case lexer.LLBRACE:
		return parser.startObjectParsing()
	default:
		return nil
	}
}

func (parser *Parser) Parse() Node {
	topLevelStatements := parser.statementList()
	if parser.currentToken != lexer.LEOF {
		panic("SyntaxError: Invalid or unexpected token")
	}
	return newCompund(topLevelStatements, []string{}, true, "")
}

func (parser *Parser) stdMathExpr() Expr {
	node := parser.term()

	for parser.currentToken >= lexer.LPLUS && parser.currentToken <= lexer.LMIN {
		token := parser.currentToken
		parser.eat(token)
		node = newBinaryOperation(node, parser.term(), token)
	}
	return node
}

func (parser *Parser) expr() Expr {
	node := parser.stdMathExpr()

	for parser.currentToken >= lexer.LGREATER && parser.currentToken <= lexer.LLESSEQUAL {
		token := parser.currentToken
		parser.eat(token)
		node = newComparison(node, parser.stdMathExpr(), token)
	}
	return node
}
