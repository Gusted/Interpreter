package interpreter

import (
	"jsInterpreter/src/context"
	"jsInterpreter/src/globals"
	"jsInterpreter/src/lexer"
	"jsInterpreter/src/parser"
	"os"
)

const (
	GConst globals.ScopeType = iota + 1
	GLet
	GVar
	GFunction
	GString
	GBuiltin
)

type NodeVisitor struct {
	Context          context.ECMA262Context
	returnValue      interface{}
	ScopeBasedValues globals.Scopebased
	ScopeBasedIndex  globals.ScopeIndex
	OwnScope         globals.Scope
	Scope            uint
	interrupt        bool
}

func (nv *NodeVisitor) visit(node parser.Node) interface{} {
	if nv.interrupt {
		return nil
	}
	switch node := node.(type) {
	case parser.Assign:
		return nv.visitAssign(node)
	case parser.EqualCheck:
		return nv.visitEqualCheck(node)
	case parser.MemberAccessFunc:
		return nv.visitMemberAccessFunc(node)
	case parser.MemberAccessProperty:
		return nv.visitMemberAccessProperty(node)
	case parser.String:
		return nv.visitString(node)
	case parser.Number:
		return nv.visitNumber(node)
	case parser.Compound:
		return nv.visitCompound(node)
	case parser.Identifier:
		return nv.visitIdentifier(node)
	case parser.BinaryOperation:
		return nv.visitBinaryOp(node)
	case parser.UnaryOperation:
		return nv.visitUnaryOperation(node)
	case parser.BinaryLeftSideOperation:
		return nv.visitBinaryLeftSideOp(node)
	case parser.FunctionCall:
		return nv.visitFunctionCall(node)
	case parser.Boolean:
		return nv.visitBoolean(node)
	case parser.Redeclare:
		return nv.visitRedeclare(node)
	case parser.IfBlock:
		return nv.visitIfBlock(node)
	case parser.ReturnStatement:
		return nv.visitReturnStmt(node)
	case parser.ObjectExpr:
		return nv.visitObject(node)
	case parser.CompareOperation:
		return nv.visitCompareOp(node)
	case parser.InDecrement:
		return nv.visitInDecrement(node)
	}
	panic("Unimplemented node type")
}

func (nv *NodeVisitor) visitInDecrement(node parser.InDecrement) (returnValue interface{}) {
	identifier := nv.visitIdentifier(node.Identifier).(globals.ScopeStruct)
	if identifier.ScopeType == GConst {
		panic("TypeError: Cannot assign to a constant.")
	}
	var incrementBy float64
	if node.Increment {
		incrementBy = 1
	} else {
		incrementBy = -1
	}
	switch identifierType := identifier.Value.(type) {
	case globals.Number:
		returnValue = identifierType.Value
		identifier.Value = identifierType.AddRaw(incrementBy)
	case globals.Boolean:
		returnValue = identifierType.ToNumber()
		identifier.Value = identifierType.ToNumber().AddRaw(incrementBy)
	default:
		returnValue = globals.NaN
		if identifier.ScopeType != GLet && identifier.ScopeType != GVar {
			identifier.ScopeType = GLet
		}

		identifier.Value = globals.NaN
	}

	nv.setVariable(node.Identifier.Identifier, identifier)
	return returnValue
}

func (nv *NodeVisitor) visitCompareOp(node parser.CompareOperation) interface{} {
	left := helperFuncToGlobalValue(nv.visit(node.Left))
	// We have to convert them to numbers.
	var result globals.Boolean

	right := nv.visit(node.Right)

	switch node.Op {
	case lexer.LGREATER:
		result = globals.Boolean{
			Value: left.GreaterCompare(right),
		}
	case lexer.LGREATEREQUAL:
		result = globals.Boolean{
			Value: left.GreaterCompare(right),
		}
	case lexer.LLESS:
		result = globals.Boolean{
			Value: left.LessCompare(right),
		}
	case lexer.LLESSEQUAL:
		result = globals.Boolean{
			Value: left.LessEqualCompare(right),
		}
	default:
		panic("Operation not implemented.")
	}

	return result
}

func (nv *NodeVisitor) visitObject(node parser.ObjectExpr) interface{} {
	tempMap := make(globals.KeyValuePairType)
	for _, entries := range node.Entries {
		value := nv.visit(entries.Value)
		tempMap[entries.Key] = value
	}
	return globals.Object{KeyValuePair: tempMap}
}

func (nv *NodeVisitor) visitReturnStmt(node parser.ReturnStatement) interface{} {
	if node.ReturnExpr != nil {
		nv.returnValue = nv.visit(node.ReturnExpr)
	}
	nv.interrupt = true
	return nil
}

func (nv *NodeVisitor) visitIfBlock(node parser.IfBlock) interface{} {
	for _, ifItem := range node.Items {
		if ifItem.IsElse {
			for _, statement := range ifItem.Action {
				nv.visit(statement)
			}
			break
		}

		conditionEpxr := nv.visit(ifItem.Condition)
		var isConditionTrue bool

	typeCheck:
		switch conditionEpxrType := conditionEpxr.(type) {
		case globals.Number: // Right side is a number.
			isConditionTrue = conditionEpxrType.IsTruthy()
		case globals.String: // Right side is a string.
			isConditionTrue = conditionEpxrType.IsTruthy()
		case globals.Boolean: // Right side is a boolean.
			isConditionTrue = conditionEpxrType.IsTruthy()
		case globals.ScopeStruct: // Right side is a variable.
			conditionEpxr = conditionEpxrType.Value
			goto typeCheck
		default: // IDK, lmao.
			panic("TypeError: invalid assignment right-hand side")
		}

		if isConditionTrue {
			for _, statement := range ifItem.Action {
				nv.visit(statement)
			}
			break
		}
	}
	return nil
}

func (nv *NodeVisitor) visitRedeclare(node parser.Redeclare) interface{} {
	variable, ok := nv.visitIdentifier(node.Identifier).(globals.ScopeStruct)
	if !ok {
		panic("Internal, should never happen. visitRedeclare: type-assertion error")
	}
	variableName := node.Identifier.Identifier
	if variable.ScopeType == GConst {
		panic("TypeError: identifier '" + variableName + "' has already been assigned")
	}

	rightFunc, isRightFunction := node.Right.(parser.Compound)
	_, isVarFunction := variable.Value.(parser.Compound)
	scopeType := variable.ScopeType

	if !isRightFunction && isVarFunction {
		scopeType = GVar
	}
	variableGlobalScope := globals.ScopeStruct{
		ScopeType: scopeType,
	}
	if isRightFunction {
		variableGlobalScope.Value = rightFunc
	} else {
		rightValue := nv.visit(node.Right)
		switch rightValueType := rightValue.(type) {
		case globals.Number: // Right side is a number.
			variableGlobalScope.Value = globals.Number{Value: rightValueType.Value}
		case globals.String: // Right side is a string.
			variableGlobalScope.Value = globals.NewString(rightValueType.Value)
		case globals.Boolean: // Right side is a boolean.
			variableGlobalScope.Value = globals.Boolean{Value: rightValueType.Value}
		case globals.ScopeStruct: // Right side is a variable.
			variableGlobalScope.Value = rightValueType.Value
		default: // IDK, lmao.
			panic("TypeError: invalid assignment right-hand side")
		}
	}

	nv.setVariable(variableName, variableGlobalScope)
	return nil
}

func (nv *NodeVisitor) visitBoolean(node parser.Boolean) interface{} {
	if node.Value {
		return globals.TrueBool
	}
	return globals.FalseBool
}

func helperFuncToGlobalValue(node interface{}) globals.GlobalValue {
	var value globals.GlobalValue
typeCheck:
	switch nodeType := node.(type) {
	case globals.String:
		value = nodeType
	case globals.Number:
		value = nodeType
	case globals.Boolean:
		value = nodeType
	case globals.ScopeStruct:
		node = nodeType.Value
		goto typeCheck
	default:
		panic("TypeError: invalid assignment right-hand side")
	}
	return value
}

func (nv *NodeVisitor) visitEqualCheck(node parser.EqualCheck) interface{} {
	// Unwrap the left node into a "Global"
	leftNode := nv.visit(node.Left)
	var value globals.GlobalValue
typeCheck:
	switch leftNodeType := leftNode.(type) {
	case globals.String:
		value = leftNodeType
	case globals.Number:
		value = leftNodeType
	case globals.Boolean:
		value = leftNodeType
	case globals.ScopeStruct:
		leftNode = leftNodeType.Value
		goto typeCheck
	}

	nvRight := nv.visit(node.Right)
	var nvRightValue interface{}
	switch nvLeftNode := nvRight.(type) {
	case globals.ScopeStruct:
		nvRightValue = nvLeftNode.Value
	default:
		nvRightValue = nvLeftNode
	}

	boolValue := value.EqualCompare(nvRightValue)
	if node.Operation == lexer.LNOTEQUAL {
		boolValue = !boolValue
	}
	return globals.Boolean{Value: boolValue}
}

func (nv *NodeVisitor) getVariable(identifier string) (globals.ScopeStruct, bool) {
	if value, ok := nv.OwnScope[identifier]; ok {
		return value, true
	}
	if scope, ok := nv.ScopeBasedIndex[identifier]; ok {
		return nv.ScopeBasedValues[scope][identifier], true
	}
	if builtIn, ok := globals.BuiltinGlobals[identifier]; ok {
		return globals.ScopeStruct{
			Value:     builtIn,
			ScopeType: GBuiltin,
		}, true
	}
	return globals.ScopeStruct{}, false
}

func (nv *NodeVisitor) setVariable(identifier string, value globals.ScopeStruct) {
	if _, ok := nv.OwnScope[identifier]; ok {
		nv.OwnScope[identifier] = value
	} else if scope, ok := nv.ScopeBasedIndex[identifier]; ok {
		nv.ScopeBasedValues[scope][identifier] = value
	} else {
		nv.OwnScope[identifier] = value
		nv.ScopeBasedIndex[identifier] = nv.Scope
	}
}

func (nv *NodeVisitor) visitMemberAccessFunc(node parser.MemberAccessFunc) interface{} {
	leftNode := nv.visit(node.Left)
	var protoFunc globals.Function
	switch leftNode := leftNode.(type) {
	case globals.Function:
		protoFunc = leftNode
	case globals.String:
		protoFunc = leftNode.GetProto(node.Identifier)
	case globals.ScopeStruct:
		// I guess?
		if leftNode.ScopeType != GBuiltin {
			panic("ReferenceError: '" + node.Identifier + "' is not defined")
		}
		protoFunc = leftNode.Value.(globals.Builtin).GetProto(node.Identifier)
	default:
		panic("TypeError: " + node.Identifier + " is not a function")
	}
	// Convert the node.arguments to 'globals'
	args := make([]interface{}, len(node.Arguments))
	for i, arg := range node.Arguments {
		args[i] = nv.visit(arg)
	}
	return protoFunc(nv.Context, args...)
}

func (nv *NodeVisitor) visitMemberAccessProperty(node parser.MemberAccessProperty) interface{} {
	leftNode := nv.visit(node.Left)
	var protoFunc globals.Function

	switch leftNode := leftNode.(type) {
	case globals.String:
		protoFunc = leftNode.GetProto(node.Identifier)
	}

	if protoFunc == nil {
		// TODO: return undefined.
		return nil
	}
	return protoFunc(nv.Context)
}

func (nv *NodeVisitor) visitString(node parser.String) interface{} {
	return globals.NewString(node.Value)
}

func (nv *NodeVisitor) visitFunctionCall(node parser.FunctionCall) interface{} {
	functionName := node.Identifier
	function, ok := nv.getVariable(functionName)
	if !ok {
		panic("ReferenceError: '" + functionName + "' is not defined")
	}
	if function.ScopeType != GFunction {
		panic("TypeError: " + functionName + " is not a function")
	}

	args := make([]interface{}, len(node.Arguments))
	for i, arg := range node.Arguments {
		args[i] = nv.visit(arg)
	}
	return nv.executeCompound(function.Value.(parser.Compound), args...)
}

func (nv *NodeVisitor) visitBinaryOp(node parser.BinaryOperation) interface{} {
	var result interface{}
	leftNode := nv.visit(node.Left)
	rightNode := nv.visit(node.Right)

	var leftString globals.String
	var rightString globals.String

	var leftNumber globals.Number
	var rightNumber globals.Number

leftTypeChecker:
	switch leftNodeType := leftNode.(type) {
	case globals.Number: // left is a number
		leftNumber = leftNodeType
	case globals.String: // left is a string
		leftString = leftNodeType
	case globals.ScopeStruct: // left is a variable
		// get the value of the variable to be assigned.
		// and check it again.
		leftNode = leftNodeType.Value
		goto leftTypeChecker
	default:
		// left is a function call or whatever.
		leftNumber = globals.NaN
	}

rightTypeChecker:
	switch rightNodeType := rightNode.(type) {
	case globals.Number: // right is a number
		rightNumber = rightNodeType
	case globals.String: // right is a string
		rightString = rightNodeType
	case globals.ScopeStruct: // right is a variable
		// get the value of the variable to be assigned.
		// and check it again.
		rightNode = rightNodeType.Value
		goto rightTypeChecker
	default:
		// left is a function call or whatever.
		leftNumber = globals.NaN
	}

	if leftString.Length != 0 || rightString.Length != 0 {
		if node.Operation == lexer.LPLUS {
			if leftString.Length != 0 {
				result = leftString.Concat(rightNode)
			} else {
				result = rightString.ConcatForward(leftNode)
			}
			return result
		}
		// if the operation is not a plus, "a" * "b" or 3 * "a" we have to return a NaN.
		return globals.NaN
	}

	switch node.Operation {
	case lexer.LPLUS:
		result = leftNumber.Add(rightNumber)
	case lexer.LMIN:
		result = leftNumber.Subtract(rightNumber)
	case lexer.LASTERISK:
		result = leftNumber.Multiply(rightNumber)
	case lexer.LDIVIDER:
		result = leftNumber.Divide(rightNumber)
	case lexer.LEXPONENT:
		result = leftNumber.Pow(rightNumber)
	default:
		panic("Binary Operation type wasn't found")
	}

	return result
}

func (nv *NodeVisitor) visitBinaryLeftSideOp(node parser.BinaryLeftSideOperation) interface{} {
	lVariableName := node.Left.Identifier
	// Check if the variable exists, and if it's not a const.
	maybeValue, ok := nv.getVariable(lVariableName)
	if !ok {
		panic("ReferenceError: '" + lVariableName + "' is not defined")
	}
	if maybeValue.ScopeType == GConst {
		panic("TypeError: invalid assignment to const '" + lVariableName + "'")
	}
	var result interface{}

	leftNode, _ := nv.getVariable(lVariableName)
	rightNode := nv.visit(node.Right)

	var leftString globals.String
	var rightString globals.String

	var leftNumber globals.Number
	var rightNumber globals.Number

	switch leftNodeType := leftNode.Value.(type) {
	case globals.Number: // left is a number
		leftNumber = leftNodeType
	case globals.String: // left is a string
		leftString = leftNodeType

	default:
		// left is a function call or whatever.
		leftNumber = globals.NaN
	}

rightTypeChecker:
	switch rightNodeType := rightNode.(type) {
	case globals.Number: // right is a number
		rightNumber = rightNodeType
	case globals.String: // right is a string
		rightString = rightNodeType
	case globals.ScopeStruct: // right is a variable
		// get the value of the variable to be assigned.
		// and check it again.
		rightNode = rightNodeType.Value
		goto rightTypeChecker
	default:
		// left is a function call or whatever.
		leftNumber = globals.NaN
	}

	if leftString.Length != 0 || rightString.Length != 0 {
		if node.Operation == lexer.LEQUALPLUS {
			if leftString.Length != 0 {
				result = leftString.Concat(rightString)
			} else {
				result = rightString.Concat(leftString)
			}
		}
	} else {
		// Check the right's side operation.
		switch node.Operation {
		case lexer.LEQUALPLUS:
			result = leftNumber.Add(rightNumber)
		case lexer.LEQUALMIN:
			result = leftNumber.Subtract(rightNumber)
		case lexer.LEQUALASTERISK:
			result = leftNumber.Multiply(rightNumber)
		case lexer.LEQUALDIVIDER:
			result = leftNumber.Divide(rightNumber)
		case lexer.LEQUALEXPONENT:
			result = leftNumber.Pow(rightNumber)
		}
	}

	nv.setVariable(lVariableName, globals.ScopeStruct{
		Value:     result,
		ScopeType: maybeValue.ScopeType,
	})
	return nil
}

func (nv *NodeVisitor) executeCompound(node parser.Compound, arguments ...interface{}) interface{} {
	if !node.Toplevel {
		nv.ScopeBasedValues[nv.Scope+1] = make(globals.Scope)
	}
	if len(node.Arguments) != 0 {
		for i, arg := range node.Arguments {
			var globalValue globals.ScopeStruct

			// Check if variable is scopeScrupt
			if carg, ok := arguments[i].(globals.ScopeStruct); ok {
				globalValue = carg
			} else {
				globalValue = globals.ScopeStruct{
					Value:     arguments[i],
					ScopeType: GLet,
				}
			}

			nv.ScopeBasedIndex[arg] = nv.Scope + 1
			nv.ScopeBasedValues[nv.Scope+1][arg] = globalValue
		}
	}

	if node.Toplevel {
		defer func() {
			nv.ScopeBasedValues[0] = nv.OwnScope
		}()

		if len(node.Children) == 1 {
			return nv.visit(node.Children[0])
		}
		for _, v := range node.Children {
			nv.visit(v)
		}
	} else {
		nv.ScopeBasedValues[nv.Scope] = nv.OwnScope
		nv.Scope++
		nv.OwnScope = nv.ScopeBasedValues[nv.Scope]
		defer func() {
			nv.Scope--
			nv.OwnScope = nv.ScopeBasedValues[nv.Scope]
		}()
		for _, v := range node.Children {
			nv.visit(v)
			if nv.interrupt {
				nv.interrupt = false
				returnValue := nv.returnValue
				nv.returnValue = nil
				return returnValue
			}
		}
	}
	return nil
}

func (nv *NodeVisitor) visitCompound(node parser.Compound) interface{} {
	if node.Toplevel {
		return nv.executeCompound(node)
	}

	maybeFunc, ok := nv.getVariable(node.Identifier)
	if ok {
		if maybeFunc.ScopeType != GFunction && maybeFunc.ScopeType != GVar {
			panic("SyntaxError: Identifier '" + node.Identifier + "' is not a function and thus cannot redefined")
		}
	}
	nv.setVariable(node.Identifier, globals.ScopeStruct{
		Value:     node,
		ScopeType: GFunction,
	})
	return nil
}

func (nv *NodeVisitor) visitAssign(node parser.Assign) interface{} {
	variableName := node.Identifier
	maybeExistingVar, existVarOk := nv.getVariable(variableName)
	if existVarOk {
		if maybeExistingVar.ScopeType == GConst {
			panic("TypeError: identifier '" + variableName + "' has already been assigned")
		}
		if node.DeclareType != parser.VVar && maybeExistingVar.ScopeType != GVar {
			panic("SyntaxError: identifier '" + variableName + "' has already been declared")
		}
	}

	rightFunc, isRightFunction := node.Right.(parser.Compound)

	var scopedDeclare globals.ScopeType
	switch node.DeclareType {
	case parser.VVar:
		scopedDeclare = GVar
	case parser.VLet:
		scopedDeclare = GLet
	case parser.VConst:
		scopedDeclare = GConst
	}
	variableGlobalScope := globals.ScopeStruct{
		ScopeType: scopedDeclare,
	}
	if isRightFunction {
		variableGlobalScope.Value = rightFunc
	} else {
		rightValue := nv.visit(node.Right)
		switch rightValueType := rightValue.(type) {
		case globals.Number: // Right side is a number.
			variableGlobalScope.Value = globals.Number{Value: rightValueType.Value}
		case globals.String: // Right side is a string.
			variableGlobalScope.Value = globals.NewString(rightValueType.Value)
		case globals.Boolean: // Right side is a boolean.
			variableGlobalScope.Value = globals.Boolean{Value: rightValueType.Value}
		case globals.Object: // Right side is an object.
			variableGlobalScope.Value = globals.Object{KeyValuePair: rightValueType.KeyValuePair}
		case globals.ScopeStruct: // Right side is a variable.
			variableGlobalScope.Value = rightValueType.Value
		default: // IDK, lmao.
			panic("TypeError: invalid assignment right-hand side")
		}
	}

	nv.setVariable(variableName, variableGlobalScope)
	return nil
}

func (nv *NodeVisitor) visitIdentifier(node parser.Identifier) interface{} {
	variableName := node.Identifier
	val, ok := nv.getVariable(variableName)
	if !ok {
		panic("ReferenceError: '" + variableName + "' is not defined")
	}
	return val
}

func (nv *NodeVisitor) visitUnaryOperation(node parser.UnaryOperation) interface{} {
	var result globals.Number
	switch node.Operation {
	case lexer.LPLUS:
		result = nv.visit(node.Expr).(globals.Number).UnaryPlus()
	case lexer.LMIN:
		result = nv.visit(node.Expr).(globals.Number).UnaryMinus()
	default:
		panic("Couldn't determine Unary Operation")
	}
	return result
}

func (nv *NodeVisitor) visitNumber(node parser.Number) interface{} {
	return globals.Number{Value: node.Value}
}

type Interpreter struct {
	scope  globals.Scopebased
	Parser *parser.Parser
	NodeVisitor
}

func NewInterpreterWithContext(parser *parser.Parser, context context.ECMA262Context) *Interpreter {
	scopes := globals.Scopebased{
		0: globals.Scope{},
	}
	return &Interpreter{
		scopes,
		parser,
		NodeVisitor{
			ScopeBasedValues: scopes,
			ScopeBasedIndex:  make(globals.ScopeIndex),
			OwnScope:         make(globals.Scope),
			Scope:            0,
			Context:          context,
			interrupt:        false,
			returnValue:      nil,
		},
	}
}

func NewInterpreter(parser *parser.Parser) *Interpreter {
	return NewInterpreterWithContext(parser, context.ECMA262Context{
		StdOut: os.Stdout,
	})
}

func (inter *Interpreter) Interpret() interface{} {
	defer func() {
		inter.scope = inter.NodeVisitor.ScopeBasedValues
	}()
	tree := inter.Parser.Parse()
	return inter.NodeVisitor.visit(tree)
}
