package globals

type GlobalValue interface {
	EqualCompare(cmpValue interface{}) bool
	GreaterCompare(cmpValue interface{}) bool
	GreaterEqualCompare(cmpValue interface{}) bool
	LessCompare(cmpValue interface{}) bool
	LessEqualCompare(cmpValue interface{}) bool

	IsTruthy() bool
}
