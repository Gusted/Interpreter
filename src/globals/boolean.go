package globals

// ref: https://tc39.es/ecma262/#sec-ecmascript-language-types-boolean-type
// Boolean should be wrapper around the `bool` type.
type Boolean struct {
	Value bool
}

// Lets pre-define some boolean values.
var (
	TrueBool  = Boolean{Value: true}
	FalseBool = Boolean{Value: false}
)

func (b Boolean) EqualCompare(cmpValue interface{}) bool {
	// Unwrap cmpValue
	switch cmpValue := cmpValue.(type) {
	case Boolean:
		return b.Value == cmpValue.Value
	case Number:
		if b.Value {
			return cmpValue.Value > 0
		}
		return cmpValue.Value == 0
	default:
		return false
	}
}

func (b Boolean) ToString() string {
	if b.Value {
		return "true"
	}
	return "false"
}

func (b Boolean) IsTruthy() bool {
	return b.Value
}

func (b Boolean) ToNumber() Number {
	if b.Value {
		return Number{Value: 1}
	}
	return Number{Value: 0}
}

func (b Boolean) GreaterCompare(cmpValue interface{}) bool {
	// Unwrap cmpValue
	switch cmpValue := cmpValue.(type) {
	case Boolean:
		return b.Value && !cmpValue.Value
	case Number:
		if b.Value {
			return cmpValue.Value <= 0
		}
		return cmpValue.Value < 0
	default:
		return false
	}
}

func (b Boolean) GreaterEqualCompare(cmpValue interface{}) bool {
	// Unwrap cmpValue
	switch cmpValue := cmpValue.(type) {
	case Boolean:
		return b.Value == cmpValue.Value
	case Number:
		if b.Value {
			return cmpValue.Value <= 1
		}
		return cmpValue.Value <= 0
	default:
		return false
	}
}

func (b Boolean) LessCompare(cmpValue interface{}) bool {
	// Unwrap cmpValue
	switch cmpValue := cmpValue.(type) {
	case Boolean:
		return !b.Value && cmpValue.Value
	case Number:
		if b.Value {
			return cmpValue.Value > 1
		}
		return cmpValue.Value > 0
	default:
		return false
	}
}

func (b Boolean) LessEqualCompare(cmpValue interface{}) bool {
	// Unwrap cmpValue
	switch cmpValue := cmpValue.(type) {
	case Boolean:
		return b.Value == cmpValue.Value
	case Number:
		if b.Value {
			return cmpValue.Value >= 1
		}
		return cmpValue.Value >= 0
	default:
		return false
	}
}
