package globals

import (
	"github.com/valyala/bytebufferpool"

	"jsInterpreter/src/context"
)

type Console struct{}

var whiteSpace = []byte(" ")

func parseArgsToString(args ...interface{}) []byte {
	b := bytebufferpool.Get()
	defer bytebufferpool.Put(b)
	lenArgs := len(args) - 1
	for i, arg := range args {
	argTypeCheck:
		switch argType := arg.(type) {
		case String:
			_, _ = b.WriteString(argType.Value)
		case Number:
			_, _ = b.WriteString(argType.ToString())
		case Boolean:
			_, _ = b.WriteString(argType.ToString())
		case ScopeStruct: // left is a variable
			// get the value of the variable to be assigned.
			// and check it again.
			arg = argType.Value
			goto argTypeCheck
		default:
			_, _ = b.WriteString("undefined")
		}
		// only write space if it's not the last argument
		if i != lenArgs {
			_, _ = b.Write(whiteSpace)
		}
	}

	return b.B
}

var newLine = []byte("\n")

func logFunc(context context.ECMA262Context, args ...interface{}) interface{} {
	parsedArgs := parseArgsToString(args...)
	// Add new line
	parsedArgs = append(parsedArgs, newLine...)
	_, err := context.StdOut.Write(parsedArgs)
	if err != nil {
		panic(err)
	}
	return nil
}

func warnFunc(context context.ECMA262Context, args ...interface{}) interface{} {
	parsedArgs := parseArgsToString(args...)
	// Add new line
	parsedArgs = append(parsedArgs, newLine...)
	_, err := context.StdOut.Write(parsedArgs)
	if err != nil {
		panic(err)
	}
	return nil
}

func errorFunc(context context.ECMA262Context, args ...interface{}) interface{} {
	parsedArgs := parseArgsToString(args...)
	// Add new line
	parsedArgs = append(parsedArgs, newLine...)
	_, err := context.StdOut.Write(parsedArgs)
	if err != nil {
		panic(err)
	}
	return nil
}

func (s Console) GetProto(identifier string) Function {
	switch identifier {
	case "log":
		return logFunc
	case "warn":
		return warnFunc
	case "error":
		return errorFunc
	default:
		return nil
	}
}
