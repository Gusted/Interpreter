package globals

import "jsInterpreter/src/context"

// ref: https://tc39.es/ecma262/#sec-ecmascript-language-types-string-type
// The String type is a constructor for strings.
type String struct {
	Value string

	Length int
}

func NewString(value string) String {
	return String{
		Value:  value,
		Length: len(value),
	}
}

func (s String) GetProto(identifier string) Function {
	switch identifier {
	case "length":
		return s.StringLen
	default:
		return nil
	}
}

// https://tc39.es/ecma262/#sec-properties-of-string-instances-length
func (s String) StringLen(_ context.ECMA262Context, args ...interface{}) interface{} {
	return Number{Value: float64(s.Length)}
}

func (s String) Concat(args ...interface{}) interface{} {
	str := s.Value
	for _, arg := range args {
		switch arg := arg.(type) {
		case String:
			str += arg.Value
		case Number:
			str += arg.ToString()
		default:
			panic("Unsupported type")
		}
	}
	return NewString(str)
}

func (s String) ConcatForward(args ...interface{}) interface{} {
	var str string
	for _, arg := range args {
		switch arg := arg.(type) {
		case String:
			str += arg.Value
		case Number:
			str += arg.ToString()
		default:
			panic("Unsupported type")
		}
	}
	return NewString(str + s.Value)
}

func (s String) EqualCompare(cmpValue interface{}) bool {
	// Unwrap cmpValue
	switch cmpValue := cmpValue.(type) {
	case String:
		return s.Value == cmpValue.Value
	case Number:
		return s.Value == cmpValue.ToString()
	default:
		return false
	}
}

func (s String) IsTruthy() bool {
	return s.Length > 0
}

func (s String) ToNumber() Number {
	if s.Value == "" {
		return Number{Value: 0}
	}
	return Number{Value: 1}
}

func (s String) GreaterCompare(cmpValue interface{}) bool {
	// Unwrap cmpValue
	switch cmpValue := cmpValue.(type) {
	case String:
		return s.Value > cmpValue.Value
	case Number:
		return s.Value > cmpValue.ToString()
	default:
		return false
	}
}

func (s String) GreaterEqualCompare(cmpValue interface{}) bool {
	// Unwrap cmpValue
	switch cmpValue := cmpValue.(type) {
	case String:
		return s.Value >= cmpValue.Value
	case Number:
		return s.Value >= cmpValue.ToString()
	default:
		return false
	}
}

func (s String) LessCompare(cmpValue interface{}) bool {
	// Unwrap cmpValue
	switch cmpValue := cmpValue.(type) {
	case String:
		return s.Value < cmpValue.Value
	case Number:
		return s.Value < cmpValue.ToString()
	default:
		return false
	}
}

func (s String) LessEqualCompare(cmpValue interface{}) bool {
	// Unwrap cmpValue
	switch cmpValue := cmpValue.(type) {
	case String:
		return s.Value <= cmpValue.Value
	case Number:
		return s.Value <= cmpValue.ToString()
	default:
		return false
	}
}
