package globals

type Builtin interface {
	GetProto(identfier string) Function
}

var BuiltinGlobals = map[string]Builtin{
	"console": Console{},
}
