package globals

type ScopeType uint8

type ScopeStruct struct {
	Value     interface{}
	ScopeType ScopeType
}

type (
	Scope      map[string]ScopeStruct
	Scopebased map[uint]Scope
	ScopeIndex map[string]uint
)
