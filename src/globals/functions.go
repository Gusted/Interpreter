package globals

import "jsInterpreter/src/context"

type Function func(context context.ECMA262Context, args ...interface{}) interface{}
