package globals

import (
	"jsInterpreter/src/utils"
	"math"
	"strconv"
)

var NaN = Number{Value: utils.GNaN}

// ref: https://tc39.es/ecma262/#sec-ecmascript-language-types-number-type
// The number should have double-precision 64-bit format IEEE 754-2019.
type Number struct {
	// All values should have the same precision and rounding mode.
	Value float64
}

// ref: https://tc39.es/ecma262/#sec-numeric-types-number-divide
// Divide number n by number d.
func (n Number) Divide(d Number) Number {
	return Number{Value: n.Value / d.Value}
}

// ref: https://tc39.es/ecma262/#sec-numeric-types-number-multiply
// Multiply number n by number d.
func (n Number) Multiply(d Number) Number {
	return Number{Value: n.Value * d.Value}
}

// ref: https://tc39.es/ecma262/#sec-numeric-types-number-subtract
// Subtract number d from number n.
func (n Number) Subtract(d Number) Number {
	return Number{Value: n.Value - d.Value}
}

// ref: https://tc39.es/ecma262/#sec-numeric-types-number-add
// Add number d by number n.
func (n Number) Add(d Number) Number {
	return Number{Value: n.Value + d.Value}
}

func (n Number) AddRaw(d float64) Number {
	return Number{Value: n.Value + d}
}

// ref: https://tc39.es/ecma262/#sec-numeric-types-bigint-unaryMinus
// Unary minus number n.
func (n Number) UnaryMinus() Number {
	return Number{Value: -n.Value}
}

// ref: none
// Unary plus number n.
func (n Number) UnaryPlus() Number {
	return Number{Value: +n.Value}
}

// ref: https://tc39.es/ecma262/#sec-numeric-types-number-exponentiate
// n to the power of d
func (n Number) Pow(d Number) Number {
	return Number{Value: math.Pow(n.Value, d.Value)}
}

func (n Number) ToString() string {
	// TODO: Make this more efficient.
	return strconv.FormatFloat(n.Value, 'f', -1, 64)
}

func (n Number) EqualCompare(cmpValue interface{}) bool {
	// Unwrap cmpValue
	switch cmpValue := cmpValue.(type) {
	case Number:
		return n.Value == cmpValue.Value
	case String:
		return n.ToString() == cmpValue.Value
	default:
		return false
	}
}

func (n Number) IsTruthy() bool {
	return n.Value != 0
}

func (n Number) GreaterCompare(cmpValue interface{}) bool {
	// Unwrap cmpValue
	switch cmpValue := cmpValue.(type) {
	case Number:
		return n.Value > cmpValue.Value
	case String:
		return n.ToString() > cmpValue.Value
	default:
		return false
	}
}

func (n Number) GreaterEqualCompare(cmpValue interface{}) bool {
	// Unwrap cmpValue
	switch cmpValue := cmpValue.(type) {
	case Number:
		return n.Value >= cmpValue.Value
	case String:
		return n.ToString() >= cmpValue.Value
	default:
		return false
	}
}

func (n Number) LessCompare(cmpValue interface{}) bool {
	// Unwrap cmpValue
	switch cmpValue := cmpValue.(type) {
	case Number:
		return n.Value < cmpValue.Value
	case String:
		return n.ToString() < cmpValue.Value
	default:
		return false
	}
}

func (n Number) LessEqualCompare(cmpValue interface{}) bool {
	// Unwrap cmpValue
	switch cmpValue := cmpValue.(type) {
	case Number:
		return n.Value <= cmpValue.Value
	case String:
		return n.ToString() <= cmpValue.Value
	default:
		return false
	}
}
