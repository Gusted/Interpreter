package globals

// ref: https://tc39.es/ecma262/#sec-object-type
// Object type is a general wrapper around all objects in the language.

type KeyValuePairType map[string]interface{}
type Object struct {
	// The keys/Value pairs of the object.

	KeyValuePair KeyValuePairType
}

func (o Object) IsTruthy() bool {
	return true
}

func (o Object) EqualCompare(cmpValue interface{}) bool {
	if objCmpValue, ok := cmpValue.(Object); ok {
		// Go trough each key/value pair of the object and compare them.
		for key, value := range o.KeyValuePair {
			if objCmpValue.KeyValuePair[key] != value {
				return false
			}
		}
	}
	return true
}

func (o Object) GreaterCompare(cmpValue interface{}) bool {
	return false
}

func (o Object) GreaterEqualCompare(cmpValue interface{}) bool {
	return false
}

func (o Object) LessCompare(cmpValue interface{}) bool {
	return false
}

func (o Object) LessEqualCompare(cmpValue interface{}) bool {
	return false
}
