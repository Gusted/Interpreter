package pipeline

// Return a struct that has a inteface of io.write but writes actually nothing.
// This is used to test that the interpreter does not panic when writing to a
// nil writer.
type nopWriter struct{}

func (nopWriter) Write(p []byte) (int, error) {
	return len(p), nil
}
