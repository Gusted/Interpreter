package pipeline

import (
	"jsInterpreter/src/context"
	"jsInterpreter/src/interpreter"
	"jsInterpreter/src/lexer"
	"jsInterpreter/src/parser"
)

func ParseJSCode(code string) interface{} {
	lexerOfCode := lexer.NewLexer(code)
	parserOfCode := parser.NewParser(lexerOfCode)
	interpreterOfCode := interpreter.NewInterpreter(parserOfCode)
	return interpreterOfCode.Interpret()
}

func debug(code string) *interpreter.Interpreter {
	lexerOfCode := lexer.NewLexer(code)
	parserOfCode := parser.NewParser(lexerOfCode)
	return interpreter.NewInterpreter(parserOfCode)
}

func debugWithContext(code string, context context.ECMA262Context) *interpreter.Interpreter {
	lexerOfCode := lexer.NewLexer(code)
	parserOfCode := parser.NewParser(lexerOfCode)
	return interpreter.NewInterpreterWithContext(parserOfCode, context)
}
