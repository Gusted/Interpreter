package pipeline

import (
	"bytes"
	"fmt"
	"math"
	"testing"

	"jsInterpreter/src/context"
	"jsInterpreter/src/globals"
	"jsInterpreter/src/interpreter"
	"jsInterpreter/src/lexer"
	"jsInterpreter/src/parser"
	"jsInterpreter/src/utils"

	"github.com/stretchr/testify/assert"
)

func TestWhiteSpaces(t *testing.T) {
	t.Parallel()

	if ParseJSCode("1 + 1").(globals.Number).Value != 2 {
		t.FailNow()
	}
}

func TestMultipleDigits(t *testing.T) {
	t.Parallel()

	if ParseJSCode("123 + 321").(globals.Number).Value != 444 {
		t.FailNow()
	}
}

func TestImportance(t *testing.T) {
	t.Parallel()

	if ParseJSCode("123 + 321 * 2").(globals.Number).Value != 765 {
		t.FailNow()
	}
}

func TestImportanceDivide(t *testing.T) {
	t.Parallel()

	result := ParseJSCode("123 + 321 / 2").(globals.Number).Value
	expected := float64(283.5)
	assert.Equal(t, expected, result)
}

func TestParan(t *testing.T) {
	t.Parallel()

	if ParseJSCode("7 + 3 * (10 / (12 / (3 + 1) - 1)) / (2 + 3) - 5 - 3 + (8)").(globals.Number).Value != 10 {
		t.FailNow()
	}
}

func TestUnaryMin(t *testing.T) {
	t.Parallel()

	if ParseJSCode("- 3").(globals.Number).Value != -3 {
		t.FailNow()
	}
}

func TestUnaryPlus(t *testing.T) {
	t.Parallel()

	if ParseJSCode("+ 3").(globals.Number).Value != 3 {
		t.FailNow()
	}
}

func TestUnaryMultiple(t *testing.T) {
	t.Parallel()

	if ParseJSCode("5 - - - + - 3").(globals.Number).Value != 8 {
		t.FailNow()
	}
}

func TestUnaryComplex(t *testing.T) {
	t.Parallel()

	if ParseJSCode("5 - - - + - (3 + 4) - +2").(globals.Number).Value != 10 {
		t.FailNow()
	}
}

func TestFunction(t *testing.T) {
	t.Parallel()

	i := debug("function a() { const b = 1 }\na()")
	i.Interpret()

	simulatedGlobalScopeStruct := globals.Scopebased{
		uint(0): {
			"a": {
				ScopeType: interpreter.GFunction,
				Value: parser.Compound{
					Arguments:  []string{},
					Identifier: "a",
					Toplevel:   false,
					Children: []parser.Node{
						parser.Assign{
							Identifier:  "b",
							DeclareType: parser.VConst,
							Right: parser.Number{
								Value: float64(1),
							},
						},
					},
				},
			},
		},
		uint(1): {
			"b": {
				ScopeType: interpreter.GConst,
				Value:     globals.Number{Value: 1},
			},
		},
	}

	assert.Equal(t, i.ScopeBasedValues, simulatedGlobalScopeStruct)
}

func TestDeclare(t *testing.T) {
	t.Parallel()

	i := debug("function a() { a = 1 }\na()")
	i.Interpret()

	simulatedGlobalScopeStruct := globals.Scopebased{
		uint(0): {
			"a": {
				Value:     globals.Number{Value: 1},
				ScopeType: interpreter.GVar,
			},
		},
		uint(1): {},
	}

	assert.Equal(t, i.ScopeBasedValues, simulatedGlobalScopeStruct)
}

func TestDeclareFunc(t *testing.T) {
	t.Parallel()

	var i *interpreter.Interpreter
	defer func() {
		expectedErrorMessage := "TypeError: a is not a function"
		if r := recover(); r != expectedErrorMessage {
			t.Errorf("\ngot  = %s\nwant = %s", r, expectedErrorMessage)
		}
		simulatedGlobalScopeStruct := globals.Scopebased{
			uint(0): {
				"a": {
					Value:     globals.Number{Value: 1},
					ScopeType: interpreter.GVar,
				},
			},
			uint(1): {},
		}

		assert.Equal(t, i.ScopeBasedValues, simulatedGlobalScopeStruct)
	}()

	i = debug("function a() { a = 1 }\na()\na()")
	i.Interpret()
}

func TestDeclareCorrectFunc(t *testing.T) {
	t.Parallel()

	ParseJSCode("function a() { a = function a() { const b = 1 } }\na()\na()")
}

func TestBinaryLeftOp(t *testing.T) {
	t.Parallel()

	i := debug("let a = 4; a /= 2")
	i.Interpret()
	simulatedGlobalScopeStruct := globals.Scopebased{
		uint(0): {
			"a": {
				Value:     globals.Number{Value: 2},
				ScopeType: interpreter.GLet,
			},
		},
	}
	assert.Equal(t, i.ScopeBasedValues, simulatedGlobalScopeStruct)
}

func TestBinaryLeftAsteriskOp(t *testing.T) {
	t.Parallel()

	i := debug("let a = 4; a *= 2")
	i.Interpret()
	simulatedGlobalScopeStruct := globals.Scopebased{
		uint(0): {
			"a": {
				Value:     globals.Number{Value: 8},
				ScopeType: interpreter.GLet,
			},
		},
	}
	assert.Equal(t, i.ScopeBasedValues, simulatedGlobalScopeStruct)
}

func TestBinaryLeftAsteriskMin(t *testing.T) {
	t.Parallel()

	i := debug("let a = 4; a -= 5")
	i.Interpret()
	simulatedGlobalScopeStruct := globals.Scopebased{
		uint(0): {
			"a": {
				Value:     globals.Number{Value: -1},
				ScopeType: interpreter.GLet,
			},
		},
	}
	assert.Equal(t, i.ScopeBasedValues, simulatedGlobalScopeStruct)
}

func TestBinaryLeftAsteriskPlus(t *testing.T) {
	t.Parallel()

	i := debug("let a = -4; a += 5")
	i.Interpret()
	simulatedGlobalScopeStruct := globals.Scopebased{
		uint(0): {
			"a": {
				Value:     globals.Number{Value: 1},
				ScopeType: interpreter.GLet,
			},
		},
	}
	assert.Equal(t, i.ScopeBasedValues, simulatedGlobalScopeStruct)
}

func TestBinaryLeftAsteriskPlusConst(t *testing.T) {
	t.Parallel()

	defer func() {
		expectedErrorMessage := "TypeError: invalid assignment to const 'a'"
		if r := recover(); r != expectedErrorMessage {
			t.Errorf("got  = %s\nwant = %s", r, expectedErrorMessage)
		}
	}()

	ParseJSCode("const a = -4; a += 5")
}

func TestNaNKeywoard(t *testing.T) {
	t.Parallel()

	i := debug("const a = NaN")
	i.Interpret()
	simulatedGlobalScopeStruct := globals.Scopebased{
		uint(0): {
			"a": {
				Value:     globals.NaN,
				ScopeType: interpreter.GConst,
			},
		},
	}

	// Because math.NaN is not equal to itself, we need to compare the string.
	simulatedSprintF := fmt.Sprintf("%#v", i.ScopeBasedValues)
	simulatedSprintT := fmt.Sprintf("%#v", simulatedGlobalScopeStruct)
	assert.Equal(t, simulatedSprintF, simulatedSprintT)
}

func TestNaNKeywoardPlus(t *testing.T) {
	t.Parallel()

	i := debug("const a = 2 + NaN")
	i.Interpret()
	simulatedGlobalScopeStruct := globals.Scopebased{
		uint(0): {
			"a": {
				Value:     globals.NaN,
				ScopeType: interpreter.GConst,
			},
		},
	}

	// Because math.NaN is not equal to itself, we need to compare the string.
	simulatedSprintF := fmt.Sprintf("%#v", i.ScopeBasedValues)
	simulatedSprintT := fmt.Sprintf("%#v", simulatedGlobalScopeStruct)
	assert.Equal(t, simulatedSprintF, simulatedSprintT)
}

func TestNaNKeywoardMin(t *testing.T) {
	t.Parallel()

	i := debug("const a = 2 - NaN")
	i.Interpret()
	simulatedGlobalScopeStruct := globals.Scopebased{
		uint(0): {
			"a": {
				Value:     globals.NaN,
				ScopeType: interpreter.GConst,
			},
		},
	}

	// Because math.NaN is not equal to itself, we need to compare the string.
	simulatedSprintF := fmt.Sprintf("%#v", i.ScopeBasedValues)
	simulatedSprintT := fmt.Sprintf("%#v", simulatedGlobalScopeStruct)
	assert.Equal(t, simulatedSprintF, simulatedSprintT)
}

func TestNaNKeywoardDivide(t *testing.T) {
	t.Parallel()

	i := debug("const a = 2 / NaN")
	i.Interpret()
	simulatedGlobalScopeStruct := globals.Scopebased{
		uint(0): {
			"a": {
				Value:     globals.NaN,
				ScopeType: interpreter.GConst,
			},
		},
	}

	// Because math.NaN is not equal to itself, we need to compare the string.
	simulatedSprintF := fmt.Sprintf("%#v", i.ScopeBasedValues)
	simulatedSprintT := fmt.Sprintf("%#v", simulatedGlobalScopeStruct)
	assert.Equal(t, simulatedSprintF, simulatedSprintT)
}

func TestNaNKeywoardMultiply(t *testing.T) {
	t.Parallel()

	i := debug("const a = 2 * NaN")
	i.Interpret()
	simulatedGlobalScopeStruct := globals.Scopebased{
		uint(0): {
			"a": {
				Value:     globals.NaN,
				ScopeType: interpreter.GConst,
			},
		},
	}

	// Because math.NaN is not equal to itself, we need to compare the string.
	simulatedSprintF := fmt.Sprintf("%#v", i.ScopeBasedValues)
	simulatedSprintT := fmt.Sprintf("%#v", simulatedGlobalScopeStruct)
	assert.Equal(t, simulatedSprintF, simulatedSprintT)
}

func TestNaNUnary(t *testing.T) {
	t.Parallel()

	i := debug("let a = 2; a += NaN")
	i.Interpret()
	simulatedGlobalScopeStruct := globals.Scopebased{
		uint(0): {
			"a": {
				Value:     globals.NaN,
				ScopeType: interpreter.GLet,
			},
		},
	}

	// Because math.NaN is not equal to itself, we need to compare the string.
	simulatedSprintF := fmt.Sprintf("%#v", i.ScopeBasedValues)
	simulatedSprintT := fmt.Sprintf("%#v", simulatedGlobalScopeStruct)
	assert.Equal(t, simulatedSprintF, simulatedSprintT)
}

func TestNaNUnaryDivide(t *testing.T) {
	t.Parallel()

	i := debug("let a = 2; a /= NaN")
	i.Interpret()
	simulatedGlobalScopeStruct := globals.Scopebased{
		uint(0): {
			"a": {
				Value:     globals.NaN,
				ScopeType: interpreter.GLet,
			},
		},
	}

	// Because math.NaN is not equal to itself, we need to compare the string.
	simulatedSprintF := fmt.Sprintf("%#v", i.ScopeBasedValues)
	simulatedSprintT := fmt.Sprintf("%#v", simulatedGlobalScopeStruct)
	assert.Equal(t, simulatedSprintF, simulatedSprintT)
}

func TestNaNUnaryMultiply(t *testing.T) {
	t.Parallel()

	i := debug("let a = 2; a *= NaN")
	i.Interpret()
	simulatedGlobalScopeStruct := globals.Scopebased{
		uint(0): {
			"a": {
				Value:     globals.NaN,
				ScopeType: interpreter.GLet,
			},
		},
	}
	// Because math.NaN is not equal to itself, we need to compare the string.
	simulatedSprintF := fmt.Sprintf("%#v", i.ScopeBasedValues)
	simulatedSprintT := fmt.Sprintf("%#v", simulatedGlobalScopeStruct)
	assert.Equal(t, simulatedSprintT, simulatedSprintF)
}

func TestNoReAssignConst(t *testing.T) {
	t.Parallel()

	defer func() {
		expectedErrorMessage := "TypeError: identifier 'a' has already been assigned"
		if r := recover(); r != expectedErrorMessage {
			t.Errorf("\ngot  = %s\nwant = %s", r, expectedErrorMessage)
		}
	}()

	ParseJSCode("const a = -4; a = 2")
}

func TestNoReAssignConstToLet(t *testing.T) {
	t.Parallel()

	defer func() {
		expectedErrorMessage := "TypeError: identifier 'a' has already been assigned"
		if r := recover(); r != expectedErrorMessage {
			t.Errorf("\ngot  = %s\nwant = %s", r, expectedErrorMessage)
		}
	}()

	ParseJSCode("const a = -4; let a = 2")
}

func TestReferenceError(t *testing.T) {
	t.Parallel()

	defer func() {
		expectedErrorMessage := "ReferenceError: 'b' is not defined"
		if r := recover(); r != expectedErrorMessage {
			t.Errorf("\ngot  = %s\nwant = %s", r, expectedErrorMessage)
		}
	}()

	ParseJSCode("const a = b;")
}

func TestReference(t *testing.T) {
	t.Parallel()

	i := debug("const a = 8; const b = a;")
	i.Interpret()
	simulatedGlobalScopeStruct := globals.Scopebased{
		uint(0): {
			"a": {
				Value:     globals.Number{Value: float64(8)},
				ScopeType: interpreter.GConst,
			},
			"b": {
				Value:     globals.Number{Value: float64(8)},
				ScopeType: interpreter.GConst,
			},
		},
	}

	assert.Equal(t, i.ScopeBasedValues, simulatedGlobalScopeStruct)
}

func TestString(t *testing.T) {
	t.Parallel()

	i := debug("const a = 'aaa'")
	i.Interpret()
	simulatedGlobalScopeStruct := globals.Scopebased{
		uint(0): {
			"a": {
				Value:     globals.NewString("aaa"),
				ScopeType: interpreter.GConst,
			},
		},
	}

	assert.Equal(t, i.ScopeBasedValues, simulatedGlobalScopeStruct)
}

func TestVariableMultiplication(t *testing.T) {
	t.Parallel()

	i := debug("let a = 2; let b = a * 2;")
	i.Interpret()
	simulatedGlobalScopeStruct := globals.Scopebased{
		uint(0): {
			"a": {
				Value:     globals.Number{Value: float64(2)},
				ScopeType: interpreter.GLet,
			},
			"b": {
				Value:     globals.Number{Value: float64(4)},
				ScopeType: interpreter.GLet,
			},
		},
	}
	assert.Equal(t, i.ScopeBasedValues, simulatedGlobalScopeStruct)
}

func TestStringVarMultiplication(t *testing.T) {
	t.Parallel()

	i := debug("let a = 'aaa'; let b = a * 2;")
	i.Interpret()
	simulatedGlobalScopeStruct := globals.Scopebased{
		uint(0): {
			"a": {
				Value:     globals.NewString("aaa"),
				ScopeType: interpreter.GLet,
			},
			"b": {
				Value:     globals.NaN,
				ScopeType: interpreter.GLet,
			},
		},
	}
	// Because math.NaN is not equal to itself, we need to compare the string.
	simulatedSprintF := fmt.Sprintf("%#v", i.ScopeBasedValues)
	simulatedSprintT := fmt.Sprintf("%#v", simulatedGlobalScopeStruct)
	assert.Equal(t, simulatedSprintF, simulatedSprintT)
}

func TestStringMultiplication(t *testing.T) {
	t.Parallel()

	i := debug("let a = 'aaa' * 2;")
	i.Interpret()
	simulatedGlobalScopeStruct := globals.Scopebased{
		uint(0): {
			"a": {
				Value:     globals.NaN,
				ScopeType: interpreter.GLet,
			},
		},
	}
	// Because math.NaN is not equal to itself, we need to compare the string.
	simulatedSprintF := fmt.Sprintf("%#v", i.ScopeBasedValues)
	simulatedSprintT := fmt.Sprintf("%#v", simulatedGlobalScopeStruct)
	assert.Equal(t, simulatedSprintF, simulatedSprintT)
}

func TestFunctionMultiplication(t *testing.T) {
	t.Parallel()

	i := debug("function a() {}\n let b = a * 2")
	i.Interpret()
	simulatedGlobalScopeStruct := globals.Scopebased{
		uint(0): {
			"a": {
				Value: parser.Compound{
					Children:   []parser.Node{},
					Toplevel:   false,
					Identifier: "a",
					Arguments:  []string{},
				},
				ScopeType: interpreter.GFunction,
			},
			"b": {
				Value:     globals.NaN,
				ScopeType: interpreter.GLet,
			},
		},
	}
	// Because math.NaN is not equal to itself, we need to compare the string.
	simulatedSprintF := fmt.Sprintf("%#v", i.ScopeBasedValues)
	simulatedSprintT := fmt.Sprintf("%#v", simulatedGlobalScopeStruct)
	assert.Equal(t, simulatedSprintF, simulatedSprintT)
}

func TestLengthString(t *testing.T) {
	t.Parallel()

	i := debug("let a = 'aaa'.length")
	i.Interpret()
	simulatedGlobalScopeStruct := globals.Scopebased{
		uint(0): {
			"a": {
				Value:     globals.Number{Value: float64(3)},
				ScopeType: interpreter.GLet,
			},
		},
	}
	assert.Equal(t, i.ScopeBasedValues, simulatedGlobalScopeStruct)
}

func TestLeftSideBinaryWithVar(t *testing.T) {
	t.Parallel()

	i := debug("let a = 'aaa'; let b = 'bbb'; a += b")
	i.Interpret()

	simulatedGlobalScopeStruct := globals.Scopebased{
		uint(0): {
			"a": {
				Value:     globals.NewString("aaabbb"),
				ScopeType: interpreter.GLet,
			},
			"b": {
				Value:     globals.NewString("bbb"),
				ScopeType: interpreter.GLet,
			},
		},
	}
	assert.Equal(t, i.ScopeBasedValues, simulatedGlobalScopeStruct)
}

func TestFunctionWithArguments(t *testing.T) {
	t.Parallel()

	// Formatted code:
	// let c = 2;
	// function a(b) {
	//  c += b;
	// }
	// a(2);
	i := debug("let c = 2; function a(b) { c += b }\n a(2);")

	i.Interpret()
	simulatedGlobalScopeStruct := globals.Scopebased{
		uint(0): {
			"a": {
				Value: parser.Compound{
					Children: []parser.Node{
						parser.BinaryLeftSideOperation{
							Left: parser.Identifier{
								Identifier: "c",
							},
							Operation: lexer.LEQUALPLUS,
							Right: parser.Identifier{
								Identifier: "b",
							},
						},
					},
					Toplevel:   false,
					Identifier: "a",
					Arguments: []string{
						"b",
					},
				},
				ScopeType: interpreter.GFunction,
			},
			"c": {
				Value:     globals.Number{Value: float64(4)},
				ScopeType: interpreter.GLet,
			},
		},
		uint(1): {
			"b": {
				Value:     globals.Number{Value: float64(2)},
				ScopeType: interpreter.GLet,
			},
		},
	}
	assert.Equal(t, i.ScopeBasedValues, simulatedGlobalScopeStruct)
}

func TestFunctionWithArgumentsAndLocal(t *testing.T) {
	t.Parallel()

	// Formatted code:
	// let b = 2;
	// let e = 0;
	// function a(b) {
	// 	let c = 3;
	// 	b += c;
	// 	e = b;
	// }
	// a(2);
	i := debug("let b = 2; let e = 0; function a(b) { let c = 3; b += c; e = b; }\n a(2);")

	i.Interpret()
	simulatedGlobalScopeStruct := globals.Scopebased{
		uint(0): {
			"a": {
				Value: parser.Compound{
					Children: []parser.Node{
						parser.Assign{
							Identifier:  "c",
							DeclareType: parser.VLet,

							Right: parser.Number{
								Value: float64(3),
							},
						},
						parser.BinaryLeftSideOperation{
							Left: parser.Identifier{
								Identifier: "b",
							},
							Operation: lexer.LEQUALPLUS,
							Right: parser.Identifier{
								Identifier: "c",
							},
						},

						parser.Redeclare{
							Identifier: parser.Identifier{
								Identifier: "e",
							},
							Right: parser.Identifier{
								Identifier: "b",
							},
						},
					},
					Toplevel:   false,
					Identifier: "a",
					Arguments: []string{
						"b",
					},
				},
				ScopeType: interpreter.GFunction,
			},
			"b": {
				Value:     globals.Number{Value: float64(2)},
				ScopeType: interpreter.GLet,
			},
			"e": {
				Value:     globals.Number{Value: float64(5)},
				ScopeType: interpreter.GLet,
			},
		},
		uint(1): {
			"b": {
				Value:     globals.Number{Value: float64(5)},
				ScopeType: interpreter.GLet,
			},
			"c": {
				Value:     globals.Number{Value: float64(3)},
				ScopeType: interpreter.GLet,
			},
		},
	}
	assert.Equal(t, i.ScopeBasedValues, simulatedGlobalScopeStruct)
}

func TestNumberPlusString(t *testing.T) {
	t.Parallel()

	i := debug("let ab = 3 + 'aaab';")
	i.Interpret()

	assert.Equal(t, i.OwnScope, globals.Scope{
		"ab": {
			Value:     globals.NewString("3aaab"),
			ScopeType: interpreter.GLet,
		},
	})
}

func TestPrint(t *testing.T) {
	t.Parallel()

	stdout := new(bytes.Buffer)
	i := debugWithContext(`console.log("hello");`, context.ECMA262Context{
		StdOut: stdout,
	})
	i.Interpret()
	assert.Equal(t, "hello\n", stdout.String())
}

func BenchmarkPrint(b *testing.B) {
	// Just for sanity
	// We're going to mock the stdout to avoid the overhead of actually printing
	// to the screen. We're not going to test the actual printing, just the
	// interpreter. We'd like to test the interpreter's performance.
	// So we're going to mock the stdout to something like /dev/null.
	// But something more cross-platform would be nice.
	f := nopWriter{}
	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		b := debugWithContext("console.log('hello');", context.ECMA262Context{
			StdOut: f,
		})
		b.Interpret()
	}
}

func BenchmarkPlus(b *testing.B) {
	for i := 0; i < b.N; i++ {
		ParseJSCode("12345 + 67890")
	}
}

func TestNewLineWithSemi(t *testing.T) {
	t.Parallel()
	i := debug("let ab = 3;\nlet c = 4;")
	i.Interpret()
	assert.Equal(t, i.OwnScope, globals.Scope{
		"ab": {
			Value:     globals.Number{Value: float64(3)},
			ScopeType: interpreter.GLet,
		},
		"c": {
			Value:     globals.Number{Value: float64(4)},
			ScopeType: interpreter.GLet,
		},
	})
}

func TestPrintVariable(t *testing.T) {
	t.Parallel()

	stdout := new(bytes.Buffer)
	i := debugWithContext("let a = 1;\nconsole.log(a);", context.ECMA262Context{
		StdOut: stdout,
	})
	i.Interpret()
	assert.Equal(t, "1\n", stdout.String())
}

func TestWarn(t *testing.T) {
	t.Parallel()

	stdout := new(bytes.Buffer)
	i := debugWithContext("console.warn('hello');", context.ECMA262Context{
		StdOut: stdout,
	})
	i.Interpret()
	assert.Equal(t, "hello\n", stdout.String())
}

func TestErrorPrint(t *testing.T) {
	t.Parallel()

	stdout := new(bytes.Buffer)
	i := debugWithContext("console.error('hello');", context.ECMA262Context{
		StdOut: stdout,
	})
	i.Interpret()
	assert.Equal(t, "hello\n", stdout.String())
}

func TestEqualNumber(t *testing.T) {
	t.Parallel()

	i := debug("let a = 8 == 8;")
	i.Interpret()
	assert.Equal(t, i.OwnScope, globals.Scope{
		"a": {
			Value:     globals.Boolean{Value: true},
			ScopeType: interpreter.GLet,
		},
	})
}

func TestEqualString(t *testing.T) {
	t.Parallel()

	i := debug(`let a = "ab" == "ab";`)
	i.Interpret()
	assert.Equal(t, i.OwnScope, globals.Scope{
		"a": {
			Value:     globals.Boolean{Value: true},
			ScopeType: interpreter.GLet,
		},
	})
}

func TestEqualRawString(t *testing.T) {
	t.Parallel()

	i := ParseJSCode(`"ab" == "ab";`)

	// Check if i returns global.Boolean{Value: true}
	assert.Equal(t, i, globals.Boolean{Value: true})
}

func TestEqualLeftNumberString(t *testing.T) {
	t.Parallel()

	i := debug("let a = 8 == '8';")
	i.Interpret()
	assert.Equal(t, i.OwnScope, globals.Scope{
		"a": {
			Value:     globals.Boolean{Value: true},
			ScopeType: interpreter.GLet,
		},
	})
}

func TestEqualRightNumberString(t *testing.T) {
	t.Parallel()

	i := debug("let a = '8' == 8;")
	i.Interpret()
	assert.Equal(t, i.OwnScope, globals.Scope{
		"a": {
			Value:     globals.Boolean{Value: true},
			ScopeType: interpreter.GLet,
		},
	})
}

func TestNewLines(t *testing.T) {
	t.Parallel()

	// Just ensure it doesn't panic.
	i := debug("function a(b) {\nconsole.log(b);\n}\na('hello');")
	i.Interpret()
}

func TestReturnResult(t *testing.T) {
	t.Parallel()

	i := debug("8 + 'a'")
	result := i.Interpret()
	assert.Equal(t, result, globals.NewString("8a"))
}

func TestInfinityMath(t *testing.T) {
	t.Parallel()

	i := debug("Infinity * Infinity")
	result := i.Interpret()
	assert.Equal(t, result, globals.Number{Value: utils.GInfinity})
}

func TestInfinityMath2(t *testing.T) {
	t.Parallel()

	i := debug("Infinity / Infinity")
	result := i.Interpret()
	// It should equal to NaN but because Nan != NaN, we can't use assert.Equal
	assert.True(t, math.IsNaN(result.(globals.Number).Value))
}

func TestIfinityUnary(t *testing.T) {
	t.Parallel()

	i := debug("-Infinity")
	result := i.Interpret()
	assert.Equal(t, result, globals.Number{Value: -utils.GInfinity})
}

func TestBoolKeyword(t *testing.T) {
	t.Parallel()

	i := debug("let a = true;")
	i.Interpret()
	assert.Equal(t, i.OwnScope, globals.Scope{
		"a": {
			Value:     globals.Boolean{Value: true},
			ScopeType: interpreter.GLet,
		},
	})
}

func TestBoolFalseKeyword(t *testing.T) {
	t.Parallel()

	i := debug("let a = false;")
	i.Interpret()
	assert.Equal(t, i.OwnScope, globals.Scope{
		"a": {
			Value:     globals.Boolean{Value: false},
			ScopeType: interpreter.GLet,
		},
	})
}

func BenchmarkInfinityMath(b *testing.B) {
	for n := 0; n < b.N; n++ {
		ParseJSCode("Infinity * Infinity")
	}
}

func TestEqualBoolean(t *testing.T) {
	t.Parallel()

	i := debug("let a = true == true;")
	i.Interpret()
	assert.Equal(t, i.OwnScope, globals.Scope{
		"a": {
			Value:     globals.Boolean{Value: true},
			ScopeType: interpreter.GLet,
		},
	})
}

func TestEqualVariable(t *testing.T) {
	t.Parallel()

	i := debug("let a = true; let b = true; let c = a == b;")
	i.Interpret()
	assert.Equal(t, i.OwnScope, globals.Scope{
		"a": {
			Value:     globals.Boolean{Value: true},
			ScopeType: interpreter.GLet,
		},
		"b": {
			Value:     globals.Boolean{Value: true},
			ScopeType: interpreter.GLet,
		},
		"c": {
			Value:     globals.Boolean{Value: true},
			ScopeType: interpreter.GLet,
		},
	})
}

func TestIfBlock(t *testing.T) {
	t.Parallel()

	i := debug("let a = false; if (a == false) { a = true; }")
	i.Interpret()
	assert.Equal(t, i.OwnScope, globals.Scope{
		"a": {
			Value:     globals.Boolean{Value: true},
			ScopeType: interpreter.GLet,
		},
	})
}

func TestIfBlockBool(t *testing.T) {
	t.Parallel()

	i := debug("let a = false; if (true) { a = true; }")
	i.Interpret()
	assert.Equal(t, i.OwnScope, globals.Scope{
		"a": {
			Value:     globals.Boolean{Value: true},
			ScopeType: interpreter.GLet,
		},
	})
}

func TestIfBlockFalse(t *testing.T) {
	t.Parallel()

	i := debug("let a = false; if (false) { a = true; }")
	i.Interpret()
	assert.Equal(t, i.OwnScope, globals.Scope{
		"a": {
			Value:     globals.Boolean{Value: false},
			ScopeType: interpreter.GLet,
		},
	})
}

func TestIfBlockElse(t *testing.T) {
	t.Parallel()

	i := debug("let a = false; if (false) { a = true; } else { a = \"Hello World!\"; }")
	i.Interpret()
	assert.Equal(t, i.OwnScope, globals.Scope{
		"a": {
			Value:     globals.NewString("Hello World!"),
			ScopeType: interpreter.GLet,
		},
	})
}

func TestIfElseBlock(t *testing.T) {
	t.Parallel()

	i := debug("let a = false; if (false) { a = true; } else if (a == false) { a = 4; }")
	i.Interpret()
	assert.Equal(t, i.OwnScope, globals.Scope{
		"a": {
			Value:     globals.Number{Value: 4},
			ScopeType: interpreter.GLet,
		},
	})
}

func BenchmarkPrintBoolVar(b *testing.B) {
	// Just for sanity
	// We're going to mock the stdout to avoid the overhead of actually printing
	// to the screen. We're not going to test the actual printing, just the
	// interpreter. We'd like to test the interpreter's performance.
	// So we're going to mock the stdout to something like /dev/null.
	// But something more cross-platform would be nice.
	f := nopWriter{}
	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		b := debugWithContext("let a = true; console.log(a);", context.ECMA262Context{
			StdOut: f,
		})
		b.Interpret()
	}
}

func TestIfElseBlockElse(t *testing.T) {
	t.Parallel()

	i := debug("let a = false; if (false) { a = true; } else if (a) { a = 4; } else { a = \"Hello World!\"; }")
	i.Interpret()
	assert.Equal(t, i.OwnScope, globals.Scope{
		"a": {
			Value:     globals.NewString("Hello World!"),
			ScopeType: interpreter.GLet,
		},
	})
}

func TestReturnStatement(t *testing.T) {
	t.Parallel()
	// Formatted:
	// function SendMessageCondition(bool, message) {
	//    if (bool) {
	//        return message;
	//    }
	//    return bool;
	//}
	//
	// let a = SendMessageCondition(true, "Hello World");
	// let b = SendMessageCondition(false, "Wouldn't be send");

	i := debug(
		"function SendMessageCondition(bool, message) {\n" +
			"    if (bool) {\n" +
			"		return message;\n" +
			"    }\n" +
			"    return bool;\n" +
			"}\n" +
			"let a = SendMessageCondition(true, \"Hello World\");\n" +
			"let b = SendMessageCondition(false, \"Wouldn't be send\");")
	i.Interpret()
	assert.Equal(t, i.OwnScope, globals.Scope{
		"SendMessageCondition": globals.ScopeStruct{
			Value: parser.Compound{
				Identifier: "SendMessageCondition",
				Children: []parser.Node{
					parser.IfBlock{
						Items: []parser.IfItem{
							{
								Condition: parser.Identifier{
									Identifier: "bool",
								},
								Action: []parser.Node{
									parser.ReturnStatement{
										ReturnExpr: parser.Identifier{
											Identifier: "message",
										},
									},
								},
								IsElse: false,
							},
						},
					},
					parser.ReturnStatement{
						ReturnExpr: parser.Identifier{
							Identifier: "bool",
						},
					},
				},
				Arguments: []string{
					"bool",
					"message",
				},
				Toplevel: false,
			},
			ScopeType: interpreter.GFunction,
		},
		"a": globals.ScopeStruct{
			Value:     globals.NewString("Hello World"),
			ScopeType: interpreter.GLet,
		},
		"b": globals.ScopeStruct{
			Value:     globals.Boolean{Value: false},
			ScopeType: interpreter.GLet,
		},
	})
}

func TestObjectString(t *testing.T) {
	t.Parallel()

	i := debug("let a = {\"a\": 1, \"b\": 2};")
	i.Interpret()

	assert.Equal(t, i.OwnScope, globals.Scope{
		"a": {
			Value: globals.Object{
				KeyValuePair: globals.KeyValuePairType{
					"a": globals.Number{Value: 1},
					"b": globals.Number{Value: 2},
				},
			},
			ScopeType: interpreter.GLet,
		},
	})
}

func TestObjectIdentifier(t *testing.T) {
	t.Parallel()

	i := debug("let a = \"aaa\"; let c = { a };")
	i.Interpret()

	assert.Equal(t, i.OwnScope, globals.Scope{
		"a": {
			Value:     globals.NewString("aaa"),
			ScopeType: interpreter.GLet,
		},
		"c": {
			Value: globals.Object{
				KeyValuePair: globals.KeyValuePairType{
					"a": globals.ScopeStruct{
						Value:     globals.NewString("aaa"),
						ScopeType: interpreter.GLet,
					},
				},
			},
			ScopeType: interpreter.GLet,
		},
	})
}

func TestObjectRegression(t *testing.T) {
	t.Parallel()

	i := debug("let d = {\n \"bx_menu_settings\": \"Настройки\",\n};\nlet a = {\n'b': 'ab',\n 'c': 8,\n d,\n};")
	i.Interpret()

	assert.Equal(t, i.OwnScope, globals.Scope{
		"d": {
			Value: globals.Object{
				KeyValuePair: globals.KeyValuePairType{
					"bx_menu_settings": globals.NewString("Настройки"),
				},
			},
			ScopeType: interpreter.GLet,
		},
		"a": {
			Value: globals.Object{
				KeyValuePair: globals.KeyValuePairType{
					"b": globals.NewString("ab"),
					"c": globals.Number{Value: 8},
					"d": globals.ScopeStruct{
						Value: globals.Object{
							KeyValuePair: globals.KeyValuePairType{
								"bx_menu_settings": globals.NewString("Настройки"),
							},
						},
						ScopeType: interpreter.GLet,
					},
				},
			},
			ScopeType: interpreter.GLet,
		},
	})
}

func TestFibonacci(t *testing.T) {
	// Test the recursive fibonacci function
	t.Parallel()

	i := debug("function fib(n) {\n" + "    if (n < 2) {\n" + "        return n;\n" + "    }\n" + "    return fib(n - 1) + fib(n - 2);\n" + "}\n" + "fib(5);")
	i.Interpret()
}

func BenchmarkFibonacci(b *testing.B) {
	for i := 0; i < b.N; i++ {
		ParseJSCode("function fib(n) {\n" + "    if (n < 2) {\n" + "        return n;\n" + "    }\n" + "    return fib(n - 1) + fib(n - 2);\n" + "}\n" + "fib(20);")
	}
}

func TestIncrement(t *testing.T) {
	t.Parallel()

	i := debug("let a = 1; a++;")
	i.Interpret()
	assert.Equal(t, i.OwnScope, globals.Scope{
		"a": {
			Value:     globals.Number{Value: 2},
			ScopeType: interpreter.GLet,
		},
	})
}

func TestDecrement(t *testing.T) {
	t.Parallel()

	i := debug("let a = 1; a--;")
	i.Interpret()
	assert.Equal(t, i.OwnScope, globals.Scope{
		"a": {
			Value:     globals.Number{Value: 0},
			ScopeType: interpreter.GLet,
		},
	})
}

func BenchmarkIncrement(b *testing.B) {
	for i := 0; i < b.N; i++ {
		ParseJSCode("let a = 1; a++;")
	}
}

func BenchmarkDecrement(b *testing.B) {
	for i := 0; i < b.N; i++ {
		ParseJSCode("let a = 1; a--;")
	}
}

func BenchmarkSlowIncrement(b *testing.B) {
	for i := 0; i < b.N; i++ {
		ParseJSCode("let a = 1; a += 1;")
	}
}

func TestBooleanIncrement(t *testing.T) {
	t.Parallel()

	i := debug("let a = true; a++;")
	i.Interpret()
	assert.Equal(t, i.OwnScope, globals.Scope{
		"a": {
			Value:     globals.Number{Value: 2},
			ScopeType: interpreter.GLet,
		},
	})
}

func TestFunctionIncrement(t *testing.T) {
	t.Parallel()

	i := debug("function a() {}; a++;")
	i.Interpret()

	simulatedOwnScope := globals.Scope{
		"a": {
			Value:     globals.NaN,
			ScopeType: interpreter.GLet,
		},
	}
	// Because math.NaN is not equal to itself, we need to compare the string.
	simulatedSprintF := fmt.Sprintf("%#v", i.OwnScope)
	simulatedSprintT := fmt.Sprintf("%#v", simulatedOwnScope)
	assert.Equal(t, simulatedSprintF, simulatedSprintT)
}

func TestExponentNumber(t *testing.T) {
	t.Parallel()

	if ParseJSCode("2**16").(globals.Number).Value != 65536 {
		t.FailNow()
	}
}

func TestExponentNumberVariable(t *testing.T) {
	t.Parallel()

	i := debug("let a = 2; a **= 16")
	i.Interpret()
	assert.Equal(t, i.OwnScope, globals.Scope{
		"a": {
			Value:     globals.Number{Value: 65536},
			ScopeType: interpreter.GLet,
		},
	})
}

func TestExponentNumberNaN(t *testing.T) {
	t.Parallel()

	i := debug("let a = NaN**128")
	i.Interpret()
	simulatedOwnScope := globals.Scope{
		"a": {
			Value:     globals.Number{Value: math.NaN()},
			ScopeType: interpreter.GLet,
		},
	}

	// Because math.NaN is not equal to itself, we need to compare the string.
	simulatedSprintF := fmt.Sprintf("%#v", i.OwnScope)
	simulatedSprintT := fmt.Sprintf("%#v", simulatedOwnScope)
	assert.Equal(t, simulatedSprintF, simulatedSprintT)
}

func BenchmarkExponentNumber(b *testing.B) {
	for i := 0; i < b.N; i++ {
		ParseJSCode("2**16")
	}
}

func TestNegativeEqual(t *testing.T) {
	t.Parallel()

	if ParseJSCode("-8 == -8").(globals.Boolean).Value != true {
		t.Fatal("-8 == -8 should be true.")
	}
}

func TestNotEqualDifferentFactors(t *testing.T) {
	t.Parallel()

	i := debug("let a = 2*8 != 16")
	i.Interpret()
	assert.Equal(t, i.OwnScope, globals.Scope{
		"a": {
			Value:     globals.Boolean{Value: false},
			ScopeType: interpreter.GLet,
		},
	})
}
