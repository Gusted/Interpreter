package main

import (
	"os"

	"jsInterpreter/cmd"
)

func main() {
	cmd.ParseAndExecuteArgs(os.Args[1:])
}
