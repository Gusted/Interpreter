package cmd

func ParseAndExecuteArgs(args []string) {
	// If os.args is empty, we execute Interactive()
	// Otherwise we can assume that we have to read and execute the file's content.
	if len(args) == 0 {
		interactive()
	} else {
		executeFile(args[0])
	}
}
