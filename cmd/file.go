package cmd

import (
	"fmt"
	"log"
	"os"

	"jsInterpreter/src/pipeline"
	"jsInterpreter/src/utils"
)

// Read the given file's contents into the given string.
func executeFile(file string) {
	contents, err := os.ReadFile(file)
	if err != nil {
		log.Fatalf("Error reading file: %s", err)
	}
	defer func() {
		if r := recover(); r != nil {
			fmt.Printf("Uncaught %s.\n", r)
		}
	}()
	pipeline.ParseJSCode(utils.UnsafeString(contents))
}
