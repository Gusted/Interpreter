package cmd

import (
	"bufio"
	"fmt"
	"os"
	"sync"

	"jsInterpreter/src/globals"
	"jsInterpreter/src/interpreter"
	"jsInterpreter/src/lexer"
	"jsInterpreter/src/parser"
)

var interpret *interpreter.Interpreter

func getInterpret(parser *parser.Parser) *interpreter.Interpreter {
	if interpret == nil {
		interpret = interpreter.NewInterpreter(parser)
		return interpret
	}
	interpret.Parser = parser
	return interpret
}

func interactive() {
	var code string
	scanner := bufio.NewScanner(os.Stdin)
	var wg sync.WaitGroup

	for {
		fmt.Print(">> ")
		if scanner.Scan() {
			code = scanner.Text()
		}
		wg.Add(1)

		// Execute new goroutine, so we can use the pipeline panic.
		go func(code string, wg *sync.WaitGroup) {
			// If the code panics, we want to print the error message,
			// so we don't just get a stack trace.
			defer func() {
				if err := recover(); err != nil {
					fmt.Println(err)
				}
				wg.Done()
			}()
			lexerOfCode := lexer.NewLexer(code)
			parserOfCode := parser.NewParser(lexerOfCode)
			interpretOfCode := getInterpret(parserOfCode)
			output := interpretOfCode.Interpret()
			if output == nil {
				output = gray + "undefined"
			} else {
				output = sanatizeInput(output)
			}
			fmt.Println(output, reset)
		}(code, &wg)

		wg.Wait()
	}
}

// Should unwrap globals and functions into read-able format and with colors.
func sanatizeInput(input interface{}) (value interface{}) {
	switch inputType := input.(type) {
	case globals.String:
		return green + "'" + inputType.Value + "'"
	case globals.Boolean:
		if inputType.Value {
			return cyan + "true"
		}
		return cyan + "false"
	case globals.Number:
		return cyan + inputType.ToString()
	case globals.ScopeStruct:
		return sanatizeInput(inputType.Value)

	default:
		return inputType
	}
}

const (
	// After each output, we want to reset the color.
	reset = "\033[0m"
	// Strings.
	green = "\033[32m"
	// Should be used for numbers + boolean.
	cyan = "\033[36m"
	// Should only be used for `undefined` and stacktraces?
	gray = "\033[90m"
)
